﻿using System;
using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels;
using Binding;
using Foundation;
using UIKit;
using UserNotifications;
using Xamarin.Forms;

namespace DemoApp.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            Forms.Init();

            LoadApplication(new App());
            SetupPermissions();

            // dummy references so that the assembly Beaconsmind.iOS is not removed by the linker since it thinks that it's not being used
            var sdk = new BeaconsmindSdk.iOS.Sdk.BeaconsmindSdk();
            var sdkInstance = DependencyService.Get<IBeaconsmindSdk>();
            DependencyService.RegisterSingleton(sdkInstance);

            var config = new BeaconsmindConfig
            {
                SuiteUri = "https://test-develop-suite.azurewebsites.net",
                AppVersion = "1",
            };
            
            sdkInstance.Initialize(config);
          
            return base.FinishedLaunching(app, options);
        }

        public override void RegisteredForRemoteNotifications(UIApplication application, NSData deviceToken)
        {
            BeaconsmindProxy.Default_.RegisterWithDeviceToken(deviceToken, PlatformTypeProxy.Apns, null);
        }
        
        public override void FailedToRegisterForRemoteNotifications(UIApplication application, NSError error)
        {
            UIAlertController.Create("Error", "Error registering push notifications", UIAlertControllerStyle.Alert);
        }
        
        private void SetupPermissions()
        {
            UNUserNotificationCenter.Current.Delegate = new UserNotificationCenterDelegate();

            var pushSettings = UIUserNotificationSettings.GetSettingsForTypes (
                UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                new NSSet ());
        
            UIApplication.SharedApplication.RegisterUserNotificationSettings (pushSettings);
            UIApplication.SharedApplication.RegisterForRemoteNotifications ();
            InvokeOnMainThread(BeaconsmindProxy.Default_.RegisterForPushNotifications);
            
            BeaconsmindProxy.Default_.RequestLocationPermissionWithCallback(b => {});
        }
    }
    
    public class UserNotificationCenterDelegate : UNUserNotificationCenterDelegate
    {
        public override void WillPresentNotification(UNUserNotificationCenter center, UNNotification notification, Action<UNNotificationPresentationOptions> completionHandler)
        {
            HandleReceived(notification.Request.Content.UserInfo);
            completionHandler(UNNotificationPresentationOptions.Alert|UNNotificationPresentationOptions.Sound);
        }

        public override void DidReceiveNotificationResponse(UNUserNotificationCenter center, UNNotificationResponse response, Action completionHandler)
        {
            response.Notification.Request.Content.UserInfo.TryGetValue(new NSString(Constants.OfferIdKey), out var offerId);
            
            if (offerId != null)
            {
                MessagingCenter.Send<object, int>(this, Constants.OfferIdKey, int.Parse(offerId.ToString()));
            }
        }
        
        private static void HandleReceived(NSDictionary contentUserInfo)
        {
            contentUserInfo.TryGetValue(new NSString(Constants.OfferIdKey), out var offerId);
            
            if (offerId != null)
            {
                DependencyService.Get<IBeaconsmindSdk>().MarkOfferAsReceived(int.Parse(offerId.ToString()));
            }
        }
    }
}

