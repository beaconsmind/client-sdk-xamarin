using Android;
using Android.Content.PM;
using AndroidX.Core.App;
using AndroidX.Core.Content;
using DemoApp.Droid;
using DemoApp.Services;
using Xamarin.Forms;
using Application = Android.App.Application;

[assembly: Dependency(typeof(PermissionService))]
namespace DemoApp.Droid
{
    public class PermissionService : IPermissionService
    {
        public void GetBeaconsRequiredPermission()
        {
            if (PermissionIsNotGranted(Manifest.Permission.BluetoothScan) || PermissionIsNotGranted(Manifest.Permission.PostNotifications) ||
                PermissionIsNotGranted(Manifest.Permission.AccessFineLocation))
            {
                ActivityCompat.RequestPermissions(MainActivity.Instance, new [] { Manifest.Permission.BluetoothScan, Manifest.Permission.AccessFineLocation, Manifest.Permission.PostNotifications }, 2);
            }
        }

        public bool AreRequiredPermissionsGranted()
        {
            return !PermissionIsNotGranted(Manifest.Permission.BluetoothScan) &&
                   !PermissionIsNotGranted(Manifest.Permission.PostNotifications) &&
                   !PermissionIsNotGranted(Manifest.Permission.AccessFineLocation);
        }
        
        private static bool PermissionIsNotGranted(string androidPermissionBluetoothScan)
        {
            return ContextCompat.CheckSelfPermission(Application.Context, androidPermissionBluetoothScan) != (int)Permission.Granted;
        }
    }
}