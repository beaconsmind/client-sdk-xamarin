﻿using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.OS;
using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels;
using BeaconsmindSdk.SdkModels.Enums;
using Xamarin.Forms;

namespace DemoApp.Droid
{
    [Activity(Label = "DemoApp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize )]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        private const string DefaultSuiteUri = "https://test-develop-suite.azurewebsites.net";
        public static MainActivity Instance;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);
            
            var sdkInstance = DependencyService.Get<IBeaconsmindSdk>();
            DependencyService.RegisterSingleton(sdkInstance);
            
            var config = new BeaconsmindConfig()
            {
                SuiteUri = DefaultSuiteUri,
                AppVersion = "1",
                UsePassiveScanning = false, 
                NotificationBadge = Resource.Drawable.xamarin_logo,
                NotificationTitle = "Beaconsmind Demo",
                NotificationText = "Active mode scanning enabled, checking for nearby beacons",
                NotificationChannelName = "beaconsmind",
            };
            
            sdkInstance.Initialize(config);
            sdkInstance.SetMinLogLevel(LogLevel.Debug);
            
            base.OnCreate(savedInstanceState);

            Instance = this;
            var offerId = Intent?.GetStringExtra(Constants.OfferIdKey);

            LoadApplication(offerId == null ? new App() : new App(int.Parse(offerId)));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions,
            [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
