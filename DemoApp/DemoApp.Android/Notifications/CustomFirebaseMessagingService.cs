using Android.App;
using Android.Content;
using Firebase.Messaging;
using BeaconsmindSdk;
using Com.Beaconsmind.Sdk;
using DemoApp.Services;
using Xamarin.Forms;

namespace DemoApp.Droid.Notifications
{
    [Service (Exported =true)]  
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class CustomFirebaseMessagingService : FirebaseMessagingService
    {
        private readonly ILocalNotificationsService _localNotificationsService;

        public CustomFirebaseMessagingService()
        {
            _localNotificationsService = new LocalNotificationsService();
        }

        public override void OnNewToken(string token)
        {
            UserManager.Instance.UpdateDeviceInfo(token);

            base.OnNewToken(token);
        }

        public override void OnMessageReceived(RemoteMessage message)
        {
            SendNotification(message);
        }
        
        public override void HandleIntent(Intent intent)
        {
            base.HandleIntent(intent);

            var offer = intent.GetStringExtra(Constants.OfferIdKey);
            if (offer != null) {
                DependencyService.Get<IBeaconsmindSdk>().MarkOfferAsReceived(int.Parse(offer));
            }
        }
        
        private void SendNotification(RemoteMessage message)
        {
            var notification = message.GetNotification();
            _localNotificationsService.ShowNotification(notification.Title, notification.Body, message.Data);
        }
    }
}