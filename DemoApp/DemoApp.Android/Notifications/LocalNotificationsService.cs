using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using AndroidX.Core.App;
using System.Collections.Generic;
using DemoApp.Droid.Notifications;
using DemoApp.Services;
using Xamarin.Forms;
using AndroidApp = Android.App.Application;

[assembly:Dependency(typeof(LocalNotificationsService))]
namespace DemoApp.Droid.Notifications
{
    public class LocalNotificationsService : ILocalNotificationsService
    {
        private const string ChannelId = "beaconsmind";
        private const string ChannelName = "beaconsmind";
        private const string ChannelDescription = "Local and push notifications messages appear in this channel";

        private int _notificationId = -1;
        private const string TitleKey = "title";
        private const string MessageKey = "message";

        private static bool _isChannelInitialized;

        private static void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                return;
            }

            var channel = new NotificationChannel(ChannelId, ChannelName, NotificationImportance.Default)
            {
                Description = ChannelDescription
            };

            var notificationManager = (NotificationManager) AndroidApp.Context.GetSystemService(Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);

            _isChannelInitialized = true;
        }

        public void ShowNotification(string title, string message, IDictionary<string, string> data)
        {
            if (!_isChannelInitialized)
            {
                CreateNotificationChannel();
            }
            
            var intent = new Intent(AndroidApp.Context, typeof(MainActivity));
            intent.PutExtra(TitleKey, title);
            intent.PutExtra(MessageKey, message);
            intent.AddFlags(ActivityFlags.ClearTop);
            
            foreach (var key in data.Keys)
            {
                intent.PutExtra(key, data[key]);
            }
            
            var  random = new Random();
            var  pushCount = random.Next(9999 - 1000) + 1000; //for multiplepushnotifications

            var pendingIntent = PendingIntent.GetActivity(AndroidApp.Context, pushCount, intent, PendingIntentFlags.Immutable);
            
            _notificationId++;

            var notificationBuilder = new NotificationCompat.Builder(AndroidApp.Context, ChannelId)
                                            .SetLargeIcon(BitmapFactory.DecodeResource(AndroidApp.Context.Resources, Resource.Mipmap.icon))
                                            .SetSmallIcon(Resource.Mipmap.icon)
                                            .SetContentTitle(title)
                                            .SetContentText(message)
                                            .SetAutoCancel(true)
                                            .SetContentIntent(pendingIntent)
                                            .SetDefaults((int)NotificationDefaults.Sound |(int)NotificationDefaults.Vibrate);

            var notificationManager = NotificationManagerCompat.From(AndroidApp.Context);
            notificationManager.Notify(_notificationId, notificationBuilder.Build());
        }
    }
}
