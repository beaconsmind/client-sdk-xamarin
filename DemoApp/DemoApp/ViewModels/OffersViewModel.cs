﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels.Responses;
using Xamarin.Forms;
using DemoApp.Views;

namespace DemoApp.ViewModels
{
    public class OffersViewModel : BaseViewModel
    {
        private OfferResponse _selectedOffer;

        public ObservableCollection<OfferResponse> Offers { get; }
        public Command LoadOffersCommand { get; }
        public Command<OfferResponse> ItemTapped { get; }

        public OffersViewModel()
        {
            Title = "Offers";
            Offers = new ObservableCollection<OfferResponse>();
            LoadOffersCommand = new Command(Execute);

            ItemTapped = new Command<OfferResponse>(OnOfferSelected);
        }

        private async void Execute()
        {
            var offersResponse = await DependencyService.Get<IBeaconsmindSdk>().LoadOffers();

            if (offersResponse.HasErrors)
            {
                await Shell.Current.DisplayAlert("Error", offersResponse.Error, "OK");
                return;
            }
            
            foreach (var offer in offersResponse.Data.Offers.Where(o => Offers.All(x => o.OfferId != x.OfferId)))
            {
                Offers.Add(offer);
            }
           
            var offersToRemove = Offers.Where(x => !offersResponse.Data.Offers.Select(y => y.OfferId).Contains(x.OfferId));
            foreach (var offer in offersToRemove)
            {
                Offers.Remove(offer);
            }

            IsBusy = false;
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public OfferResponse SelectedItem
        {
            get => _selectedOffer;
            set
            {
                SetProperty(ref _selectedOffer, value);
                OnOfferSelected(value);
            }
        }

        async void OnOfferSelected(OfferResponse offer)
        {
            if (offer == null)
                return;

            await Shell.Current.GoToAsync($"{nameof(OfferDetailPage)}?{nameof(OfferDetailViewModel.OfferId)}={offer.OfferId}");
        }
    }
}
