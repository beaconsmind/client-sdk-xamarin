using BeaconsmindSdk;
using DemoApp.Services;
using DemoApp.Views;
using Xamarin.Forms;

namespace DemoApp.ViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        public Command ProfileDetailsCommand { get; }
        public Command LogoutCommand { get; }

        public ProfileViewModel()
        {
            Title = "Profile Details";
            ProfileDetailsCommand = new Command(ExecuteLoadProfileCommand);
            LogoutCommand = new Command(OnLogout);
        }
        
        async void ExecuteLoadProfileCommand()
        {
            await Shell.Current.GoToAsync($"{nameof(ProfileDetailPage)}");
        }
        
        private async void OnLogout()
        {
            var result = await DependencyService.Get<IBeaconsmindSdk>().Logout();

            if (result.HasErrors) 
            {
                await Shell.Current.DisplayAlert("Error", result.Error, "OK");
            }
            else
            {
                await Shell.Current.GoToAsync($"//{nameof(LoginPage)}");
            }
        }
        
        public void OnAppearing()
        {
            if (Device.RuntimePlatform == Device.Android)
            {
                DependencyService.Get<IPermissionService>().GetBeaconsRequiredPermission();
            }
            
            DependencyService.Get<IBeaconsmindSdk>().StartListeningBeacons();

            IsBusy = true;
        }
    }
}