﻿using BeaconsmindSdk;
using DemoApp.Services;
using DemoApp.Views;
using Xamarin.Forms;

namespace DemoApp.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        private string email;
        private string password;
        
        public Command LoginCommand { get; }

        public LoginViewModel()
        {
            LoginCommand = new Command(OnLoginClicked);
        }
        
        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }

        private async void OnLoginClicked(object obj)
        {
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password))
            {
                await Shell.Current.DisplayAlert("Error", "Email or password is missing.", "OK");
                return;
            }
            
            var result = await DependencyService.Get<IBeaconsmindSdk>().Login(email, password);
            
            if (!result.HasErrors)
            {
                if (Device.RuntimePlatform == Device.Android)
                {
                    DependencyService.Get<IPermissionService>().GetBeaconsRequiredPermission();
                }

                DependencyService.Get<IBeaconsmindSdk>().StartListeningBeacons();

                await Shell.Current.GoToAsync($"//{nameof(ProfilePage)}");
            } 
            else
            {
                await Shell.Current.DisplayAlert("Error", result.Error, "OK");
            }
        }
        
        public void OnAppearing()
        {
            Email = "";
            Password = "";
        }
    }
}

