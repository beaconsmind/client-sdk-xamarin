using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels.Responses;
using Xamarin.Forms;

namespace DemoApp.ViewModels
{
    public class BeaconsViewModel : BaseViewModel
    {
        public ObservableCollection<BeaconContactsSummary> Beacons { get; }
        public Command LoadItemsCommand { get; }

        public BeaconsViewModel()
        {
            Title = "Beacons";
            Beacons = new ObservableCollection<BeaconContactsSummary>();
            LoadItemsCommand = new Command(ExecuteLoadItemsCommand);
        }

        void ExecuteLoadItemsCommand()
        {
            DependencyService.Get<IBeaconsmindSdk>().StartListeningBeacons();

            IsBusy = true;
            
            Device.StartTimer (new TimeSpan (0, 0, 1), () =>
            {
                var result = DependencyService.Get<IBeaconsmindSdk>().GetBeaconsSummary();

                if (result == null || result.Count <= 0) return true;

                RemoveBeacons(result);
                UpdateChangedBeacons(result);
                AddNewBeacons(result);

                return true; // runs again, or false to stop
            });

            IsBusy = false;
        }

        private void AddNewBeacons(IEnumerable<BeaconContactsSummary> result)
        {
            foreach (var beaconContactsSummary in result.Where(beaconContactsSummary => Beacons.All(x => beaconContactsSummary.Uuid != x.Uuid)))
            {
                Beacons.Add(beaconContactsSummary);
            }
        }

        private void UpdateChangedBeacons(List<BeaconContactsSummary> result)
        {
            for (var i = 0; i < Beacons.Count; i++)
            {
                var changedBeacon =  result.FirstOrDefault(x => x.Uuid == Beacons[i].Uuid && x.TimesContacted != Beacons[i].TimesContacted);

                if (changedBeacon != null)
                {
                    Beacons[i] = changedBeacon;
                }
            }

        }

        private void RemoveBeacons(List<BeaconContactsSummary> result)
        {
            var beaconsToRemove = Beacons.Where(x => !result.Select(y => y.Uuid).Contains(x.Uuid));

            foreach (var beaconContactsSummary in beaconsToRemove)
            {
                Beacons.Remove(beaconContactsSummary);
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
        }
    }
    
    public class ObjectPropertyToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var myObjectPropertyValue = (bool)value;
            return myObjectPropertyValue ? Color.Green : Color.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();

        }
    }
}