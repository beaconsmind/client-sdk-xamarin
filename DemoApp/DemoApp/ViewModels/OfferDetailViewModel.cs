﻿using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels.Responses;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace DemoApp.ViewModels
{
    [QueryProperty(nameof(OfferId), nameof(OfferId))]
    public class OfferDetailViewModel : BaseViewModel
    {
        public OfferDetailViewModel()
        {
            Title = "Offer";
            CallToActionCommand = new Command(OnVisitLink);
            RedeemCommand = new Command(OnRedeem);
        }
        
        public Command CallToActionCommand { get; }
        public Command RedeemCommand { get; }

        private OfferResponse _offer;
        private int _offerId;
        private bool _showRedeemButton;
        private bool _showCtaButton;

        public OfferResponse Offer 
        {
            get => _offer;
            set
            {
                _offer = value;
                ShowRedeemButton = _offer.IsRedeemable.HasValue && _offer.IsRedeemable.Value && _offer.IsRedeemed.HasValue && !_offer.IsRedeemed.Value;
                ShowCtaButton = _offer.CallToAction?.Title != null;
                OnPropertyChanged();
            }
        }
        
        public bool ShowRedeemButton 
        {
            get => _showRedeemButton;
            set
            {
                _showRedeemButton = value;
                OnPropertyChanged();
            }
        }
        
        public bool ShowCtaButton
        {
            get => _showCtaButton;
            set
            {
                _showCtaButton = value;
                OnPropertyChanged();
            }
        }
        
        public int OfferId
        {
            get
            {
                return _offerId;
            }
            set
            {
                _offerId = value;
                LoadItemId(value);
            }
        }

        private async void OnRedeem()
        {
            var answer = await Shell.Current.DisplayAlert("Redeem offer", "Would you like to redeem a offer?", "Yes", "No");

            if (!answer)
            {
                return;
            }
            
            var result = await DependencyService.Get<IBeaconsmindSdk>().MarkOfferAsRedeemed(Offer.OfferId);

            if (result.HasErrors)
            {
                await Shell.Current.DisplayAlert("Error", result.Error, "OK");
            }
            else
            {
                await Shell.Current.DisplayAlert("Success", "Offer redeemed", "OK");
            }

            LoadItemId(OfferId);
        }
        
        private async void OnVisitLink()
        {
            var result = await DependencyService.Get<IBeaconsmindSdk>().MarkOfferAsClicked(Offer.OfferId);

            if (result.HasErrors)
            {
                await Shell.Current.DisplayAlert("Error", result.Error, "OK");
            }
            
            await Browser.OpenAsync(Offer.CallToAction.Link);
        }

        private async void LoadItemId(int offerId)
        {
            await DependencyService.Get<IBeaconsmindSdk>().MarkOfferAsRead(offerId);

            var offerResponse = await DependencyService.Get<IBeaconsmindSdk>().LoadOffer(offerId);

            if (!offerResponse.HasErrors)
            {
                var offer = offerResponse.Data;

                Offer = offer;
                ShowRedeemButton = offer.IsRedeemable.HasValue && offer.IsRedeemable.Value &&
                                   offer.IsRedeemed.HasValue && !offer.IsRedeemed.Value;

                ShowCtaButton = offer.CallToAction != null;
            }
            else
            {
                await Shell.Current.DisplayAlert("Error", offerResponse.Error, "OK");
            }
        }
    }
}

