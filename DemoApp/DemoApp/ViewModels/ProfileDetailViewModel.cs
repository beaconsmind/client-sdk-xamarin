﻿using System.Threading.Tasks;
using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels.Requests;
using BeaconsmindSdk.SdkModels.Responses;
using Xamarin.Forms;

namespace DemoApp.ViewModels 
{
    public class ProfileDetailViewModel : BaseViewModel
    {
        private ProfileResponse _profile;
        public ProfileResponse Profile 
        {
            get => _profile;
            set
            {
                _profile = value;
                OnPropertyChanged();
            }
        }
        
        public Command LoadProfileCommand { get; }
        public Command EditCommand { get; }

        public ProfileDetailViewModel()
        {
            Title = "Profile";
            LoadProfileCommand = new Command(async () => await ExecuteLoadProfileCommand());
            EditCommand = new Command(OnSave);
        }

        async Task ExecuteLoadProfileCommand()
        {
            IsBusy = true;
            
            var profile = await DependencyService.Get<IBeaconsmindSdk>().GetAccount();

            if (!profile.HasErrors)
            {
                Profile = profile.Data;
            } 
            else
            {
                await Shell.Current.DisplayAlert("Error", profile.Error, "OK");
            }
            
            IsBusy = false;
        }
        
        private async void OnSave()
        {
            var result = await DependencyService.Get<IBeaconsmindSdk>().UpdateAccount(new UpdateProfileRequest(Profile));

            if (result.HasErrors) 
            {
                await Shell.Current.DisplayAlert("Error", result.Error, "OK");
            } 
            else {
                await Shell.Current.DisplayAlert("Success", result.Error, "Profile is updated");
            }
        }
        
        public void OnAppearing()
        {
            IsBusy = true;
        }
    }
}
