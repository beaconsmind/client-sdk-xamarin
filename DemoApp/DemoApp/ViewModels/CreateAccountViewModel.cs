using BeaconsmindSdk;
using BeaconsmindSdk.SdkModels.Requests;
using DemoApp.Services;
using DemoApp.Views;
using Xamarin.Forms;

namespace DemoApp.ViewModels
{
    public class CreateAccountViewModel : BaseViewModel
    {
        private string firstName;
        private string lastName;
        private string email;
        private string password;
        private string confirmPassword;
        
        public Command CreateAccountCommand { get; }

        public CreateAccountViewModel()
        {
            CreateAccountCommand = new Command(OnCreateAccountClicked);
        }
        
        public string FirstName
        {
            get => firstName;
            set => SetProperty(ref firstName, value);
        }
        
        public string LastName
        {
            get => lastName;
            set => SetProperty(ref lastName, value);
        }

        public string Email
        {
            get => email;
            set => SetProperty(ref email, value);
        }

        public string Password
        {
            get => password;
            set => SetProperty(ref password, value);
        }
        
        public string ConfirmPassword
        {
            get => confirmPassword;
            set => SetProperty(ref confirmPassword, value);
        }

        private async void OnCreateAccountClicked(object obj)
        {
            if (SomeFieldsAreMissing())
            {
                await Shell.Current.DisplayAlert("Error", "All fields must be filled", "OK");
                return;
            }

            if (Password != ConfirmPassword)
            {
                await Shell.Current.DisplayAlert("Error", "Password and Confirm password are not the same", "OK");
                return;
            }
            
            var request = new CreateUserRequest()
            {
                FirstName = FirstName,
                LastName = LastName,
                UserName = Email,
                Password = Password,
                ConfirmPassword = ConfirmPassword
            };
            
            var result = await DependencyService.Get<IBeaconsmindSdk>().SignUp(request);

            if (!result.HasErrors)
            {
                if (Device.RuntimePlatform == Device.Android)
                {
                    DependencyService.Get<IPermissionService>().GetBeaconsRequiredPermission();
                }
                
                // Prefixing with `//` switches to a different navigation stack instead of pushing to the active one
                await Shell.Current.GoToAsync($"//{nameof(ProfilePage)}");
            } else
            {
                await Shell.Current.DisplayAlert("Error", "Sign up failed", "OK");
            }
        }

        private bool SomeFieldsAreMissing()
        {
            return string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(Email)
                   || string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(ConfirmPassword);
        }
    }
}