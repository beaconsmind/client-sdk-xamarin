﻿using DemoApp.Views;
using Xamarin.Forms;

namespace DemoApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(OfferDetailPage), typeof(OfferDetailPage));
            Routing.RegisterRoute(nameof(ProfileDetailPage), typeof(ProfileDetailPage));
        }
    }
}

