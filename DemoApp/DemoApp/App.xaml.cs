﻿using BeaconsmindSdk;
using DemoApp.ViewModels;
using DemoApp.Views;
using Xamarin.Forms;

namespace DemoApp
{
    public partial class App : Application
    {
        private int _offerId;
        
        public App (int offerId = -1)
        {
            InitializeComponent();
            
            MainPage = new AppShell();
            
            MessagingCenter.Subscribe<object, int> (this, Constants.OfferIdKey, (s, id) =>
            {
                _offerId = id;
                OnStart();
            });


            _offerId = offerId;
        }
        
        protected override async void OnStart ()
        {            
            var token = DependencyService.Get<IBeaconsmindSdk>().GetAccessToken();
                
            if (token != null)
            {
                if (_offerId != -1)
                {
                    await Shell.Current.GoToAsync($"//{nameof(OffersPage)}");
                    await Shell.Current.GoToAsync($"{nameof(OfferDetailPage)}?{nameof(OfferDetailViewModel.OfferId)}={_offerId}");
                }
                else
                {
                    await Shell.Current.GoToAsync($"//{nameof(ProfilePage)}");
                }
            }
            else
            {
                await Shell.Current.GoToAsync($"//{nameof(LoginPage)}");
            }
        }

        protected override void OnSleep ()
        {
        }

        protected override void OnResume ()
        {
        }
    }
}

