﻿using System.Collections.Generic;

namespace DemoApp.Services
{
    public interface ILocalNotificationsService
    {
        void ShowNotification(string title, string message, IDictionary<string, string> data);
    }
}
