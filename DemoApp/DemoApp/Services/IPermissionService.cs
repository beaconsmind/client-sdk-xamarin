namespace DemoApp.Services
{
    public interface IPermissionService
    {
        void GetBeaconsRequiredPermission();
    }
}