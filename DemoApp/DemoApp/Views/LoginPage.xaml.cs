﻿using System;
using DemoApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        LoginViewModel _loginViewModel;

        public LoginPage()
        {
            InitializeComponent();
            BindingContext = _loginViewModel = new LoginViewModel();
        }

        private async void OnCreateAccountClicked(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync($"//{nameof(CreateAccountPage)}");
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _loginViewModel.OnAppearing();
        }
    }
}
