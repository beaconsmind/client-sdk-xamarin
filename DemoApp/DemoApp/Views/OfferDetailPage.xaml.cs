﻿using DemoApp.ViewModels;
using Xamarin.Forms;

namespace DemoApp.Views
{
    public partial class OfferDetailPage : ContentPage
    {
        public OfferDetailPage()
        {
            InitializeComponent();
            BindingContext = new OfferDetailViewModel();
        }
    }
}
