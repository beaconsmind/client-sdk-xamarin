using DemoApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProfilePage : ContentPage
    {
        ProfileViewModel _profileViewModel;

        public ProfilePage()
        {
            InitializeComponent();
            BindingContext = _profileViewModel = new ProfileViewModel();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _profileViewModel.OnAppearing();
        }
    }
}