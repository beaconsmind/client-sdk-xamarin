﻿using System;
using DemoApp.ViewModels;
using Xamarin.Forms;

namespace DemoApp.Views
{
    public partial class OffersPage : ContentPage
    {
        OffersViewModel _viewModel;

        public OffersPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new OffersViewModel();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}
