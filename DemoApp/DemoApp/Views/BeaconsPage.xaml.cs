using DemoApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DemoApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BeaconsPage : ContentPage
    {
        BeaconsViewModel _viewModel;
        
        public BeaconsPage()
        {
            InitializeComponent();
            
            BindingContext = _viewModel = new BeaconsViewModel();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }
    }
}