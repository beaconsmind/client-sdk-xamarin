﻿using DemoApp.ViewModels;
using Xamarin.Forms;

namespace DemoApp.Views
{
    public partial class ProfileDetailPage : ContentPage
    {
        ProfileDetailViewModel _detailViewModel;

        public ProfileDetailPage()
        {
            InitializeComponent();
            
            BindingContext = _detailViewModel = new ProfileDetailViewModel();
        }
        
        protected override void OnAppearing()
        {
            base.OnAppearing();
            _detailViewModel.OnAppearing();
        }
    }
}
