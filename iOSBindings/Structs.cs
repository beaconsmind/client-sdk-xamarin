﻿using ObjCRuntime;

namespace Binding
{
    [Native]
    public enum ErrorType : long
    {
        Unknown = 0,
        WrongSelf = 1,
        NotStarted = 2,
        NeedsMainThread = 3,
        Generic = 4,
        Location = 5,
        Network = 6,
        Parse = 7,
        Apierror = 8,
        InvalidContext = 9,
        InvalidStore = 10,
        InvalidImage = 11
    }

    [Native]
    public enum LogLevelProxy : long
    {
        Silent = 0,
        Error = 1,
        Warning = 2,
        Info = 3,
        Debug = 4
    }

    [Native]
    public enum PlatformTypeProxy : long
    {
        Unknown = 0,
        Fcm = 1,
        Apns = 2
    }
}