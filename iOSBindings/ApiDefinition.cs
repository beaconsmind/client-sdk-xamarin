﻿using System;
// using BeaconsmindProxy;
using Foundation;
using ObjCRuntime;

namespace Binding
{
    // @interface APIContextProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy15APIContextProxy")]
    [DisableDefaultCtor]
    interface APIContextProxy
    {
        // @property (readonly, copy, nonatomic) NSString * _Nonnull hostname;
        [Export("hostname")]
        string Hostname { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull accessToken;
        [Export("accessToken")]
        string AccessToken { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull userID;
        [Export("userID")]
        string UserID { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable tokenType;
        [NullAllowed, Export("tokenType")]
        string TokenType { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable expiresIn;
        [NullAllowed, Export("expiresIn")]
        string ExpiresIn { get; }
    }

    // @interface BeaconContactsSummaryProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy26BeaconContactsSummaryProxy")]
    [DisableDefaultCtor]
    interface BeaconContactsSummaryProxy
    {
        // @property (readonly, copy, nonatomic) NSString * _Nonnull store;
        [Export("store")]
        string Store { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull name;
        [Export("name")]
        string Name { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull uuid;
        [Export("uuid")]
        string Uuid { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull major;
        [Export("major")]
        string Major { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull minor;
        [Export("minor")]
        string Minor { get; }

        // @property (readonly, nonatomic, strong) NSNumber * _Nullable rssi;
        [NullAllowed, Export("rssi", ArgumentSemantic.Strong)]
        NSNumber Rssi { get; }

        // @property (readonly, nonatomic, strong) NSNumber * _Nullable averageRSSI;
        [NullAllowed, Export("averageRSSI", ArgumentSemantic.Strong)]
        NSNumber AverageRSSI { get; }

        // @property (readonly, nonatomic, strong) NSNumber * _Nullable timestamp;
        [NullAllowed, Export("timestamp", ArgumentSemantic.Strong)]
        NSNumber Timestamp { get; }

        // @property (readonly, nonatomic) NSInteger frequency;
        [Export("frequency")]
        nint Frequency { get; }

        // @property (readonly, nonatomic) NSInteger contactCount;
        [Export("contactCount")]
        nint ContactCount { get; }

        // @property (readonly, nonatomic) BOOL isInRange;
        [Export("isInRange")]
        bool IsInRange { get; }
    }

    // @protocol BeaconsmindDelegateProxy
    /*
  Check whether adding [Model] to this declaration is appropriate.
  [Model] is used to generate a C# class that implements this protocol,
  and might be useful for protocols that consumers are supposed to implement,
  since consumers can subclass the generated class instead of implementing
  the generated interface. If consumers are not supposed to implement this
  protocol, then [Model] is redundant and will generate code that will never
  be used.
*/
    [BaseType(typeof(NSObject))]
    [Model]
    [Protocol(Name = "_TtP16BeaconsmindProxy24BeaconsmindDelegateProxy_")]
    interface BeaconsmindDelegateProxy
    {
        // @required -(void)beaconsmind:(BeaconsmindProxy * _Nonnull)beaconsmind onContextChanged:(APIContextProxy * _Nullable)context;
        [Abstract]
        [Export("beaconsmind:onContextChanged:")]
        void OnContextChanged(BeaconsmindProxy beaconsmind, [NullAllowed] APIContextProxy context);
    }

    // @interface BeaconsmindProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy16BeaconsmindProxy")]
    interface BeaconsmindProxy
    {
        // @property (readonly, getter = default, nonatomic, strong, class) BeaconsmindProxy * _Nonnull default_;
        [Static]
        [Export("default_", ArgumentSemantic.Strong)]
        BeaconsmindProxy Default_ { [Bind("default")] get; }

        // -(BOOL)startWithDelegate:(id<BeaconsmindDelegateProxy> _Nonnull)delegate appVersion:(NSString * _Nonnull)appVersion hostname:(NSString * _Nonnull)hostname __attribute__((warn_unused_result("")));
        [Export("startWithDelegate:appVersion:hostname:")]
        bool StartWithDelegate(BeaconsmindDelegateProxy @delegate, string appVersion, string hostname);

        // -(void)loginWithUsername:(NSString * _Nonnull)username password:(NSString * _Nonnull)password completion:(void (^ _Nullable)(APIContextProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("loginWithUsername:password:completion:")]
        void LoginWithUsername(string username, string password, [NullAllowed] Action<APIContextProxy, SDKApiErrorProxy> completion);

        // -(void)signUpWithUsername:(NSString * _Nonnull)username firstName:(NSString * _Nonnull)firstName lastName:(NSString * _Nonnull)lastName password:(NSString * _Nonnull)password confirmPassword:(NSString * _Nonnull)confirmPassword avatarThumbnailURL:(NSString * _Nullable)avatarThumbnailURL avatarURL:(NSString * _Nullable)avatarURL language:(NSString * _Nullable)language gender:(NSString * _Nullable)gender favoriteStoreID:(NSNumber * _Nullable)favoriteStoreID birthDate:(NSDate * _Nullable)birthDate completion:(void (^ _Nullable)(APIContextProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("signUpWithUsername:firstName:lastName:password:confirmPassword:avatarThumbnailURL:avatarURL:language:gender:favoriteStoreID:birthDate:completion:")]
        void SignUpWithUsername(string username, string firstName, string lastName, string password, string confirmPassword, [NullAllowed] string avatarThumbnailURL, [NullAllowed] string avatarURL, [NullAllowed] string language, [NullAllowed] string gender, [NullAllowed] NSNumber favoriteStoreID, [NullAllowed] NSDate birthDate, [NullAllowed] Action<APIContextProxy, SDKApiErrorProxy> completion);

        // -(void)logoutWithCompletion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("logoutWithCompletion:")]
        void LogoutWithCompletion([NullAllowed] Action<bool, SDKApiErrorProxy> completion);

        // -(APIContextProxy * _Nullable)getAPIContext __attribute__((warn_unused_result("")));
        [NullAllowed, Export("getAPIContext")]
        // [Verify(MethodToProperty)]
        APIContextProxy APIContext { get; }

        // -(void)updateHostnameWithHostname:(NSString * _Nonnull)hostname;
        [Export("updateHostnameWithHostname:")]
        void UpdateHostnameWithHostname(string hostname);

        // -(void)importAccountWithId:(NSString * _Nonnull)id email:(NSString * _Nonnull)email firstName:(NSString * _Nullable)firstName lastName:(NSString * _Nullable)lastName birthDate:(NSDate * _Nullable)birthDate language:(NSString * _Nullable)language gender:(NSString * _Nullable)gender completion:(void (^ _Nullable)(APIContextProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("importAccountWithId:email:firstName:lastName:birthDate:language:gender:completion:")]
        void ImportAccountWithId(string id, string email, [NullAllowed] string firstName, [NullAllowed] string lastName, [NullAllowed] NSDate birthDate, [NullAllowed] string language, [NullAllowed] string gender, [NullAllowed] Action<APIContextProxy, SDKApiErrorProxy> completion);

        // -(SDKApiErrorProxy * _Nullable)startListeningBeacons __attribute__((warn_unused_result("")));
        [Export("startListeningBeacons")]
        // [Verify (MethodToProperty)]
        SDKApiErrorProxy StartListeningBeacons();

        // -(void)stopListeningBeacons;
        [Export("stopListeningBeacons")]
        void StopListeningBeacons();

        // -(NSArray<BeaconContactsSummaryProxy *> * _Nonnull)getBeaconsContactsSummary __attribute__((warn_unused_result("")));
        [Export("getBeaconsContactsSummary")]
        // [Verify(MethodToProperty)]
        BeaconContactsSummaryProxy[] BeaconsContactsSummary { get; }

        // -(void)registerForPushNotifications;
        [Export("registerForPushNotifications")]
        void RegisterForPushNotifications();

        // -(void)registerWithDeviceToken:(NSData * _Nonnull)deviceToken platformType:(enum PlatformTypeProxy)platformType completion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("registerWithDeviceToken:platformType:completion:")]
        void RegisterWithDeviceToken(NSData deviceToken, PlatformTypeProxy platformType, [NullAllowed] Action<bool, SDKApiErrorProxy> completion);

        // -(void)requestLocationPermissionWithCallback:(void (^ _Nullable)(BOOL))callback;
        [Export("requestLocationPermissionWithCallback:")]
        void RequestLocationPermissionWithCallback([NullAllowed] Action<bool> callback);

        // -(void)setMinLogLevelWithLevel:(enum LogLevelProxy)level;
        [Export("setMinLogLevelWithLevel:")]
        void SetMinLogLevelWithLevel(LogLevelProxy level);

        // -(void)getProfileWithCompletion:(void (^ _Nullable)(ProfileResponseProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("getProfileWithCompletion:")]
        void GetProfileWithCompletion([NullAllowed] Action<ProfileResponseProxy, SDKApiErrorProxy> completion);

        // -(void)updateProfileWithFirstName:(NSString * _Nonnull)firstName lastName:(NSString * _Nonnull)lastName avatarThumbnailURL:(NSString * _Nullable)avatarThumbnailURL avatarURL:(NSString * _Nullable)avatarURL birthDate:(NSDate * _Nullable)birthDate city:(NSString * _Nullable)city country:(NSString * _Nullable)country disablePushNotifications:(NSNumber * _Nullable)disablePushNotifications favoriteStoreID:(NSNumber * _Nullable)favoriteStoreID gender:(NSString * _Nullable)gender houseNumber:(NSString * _Nullable)houseNumber landlinePhone:(NSString * _Nullable)landlinePhone language:(NSString * _Nullable)language phoneNumber:(NSString * _Nullable)phoneNumber street:(NSString * _Nullable)street zipCode:(NSString * _Nullable)zipCode completion:(void (^ _Nullable)(ProfileResponseProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("updateProfileWithFirstName:lastName:avatarThumbnailURL:avatarURL:birthDate:city:country:disablePushNotifications:favoriteStoreID:gender:houseNumber:landlinePhone:language:phoneNumber:street:zipCode:completion:")]
        void UpdateProfileWithFirstName(string firstName, string lastName, [NullAllowed] string avatarThumbnailURL, [NullAllowed] string avatarURL, [NullAllowed] NSDate birthDate, [NullAllowed] string city, [NullAllowed] string country, [NullAllowed] NSNumber disablePushNotifications, [NullAllowed] NSNumber favoriteStoreID, [NullAllowed] string gender, [NullAllowed] string houseNumber, [NullAllowed] string landlinePhone, [NullAllowed] string language, [NullAllowed] string phoneNumber, [NullAllowed] string street, [NullAllowed] string zipCode, [NullAllowed] Action<ProfileResponseProxy, SDKApiErrorProxy> completion);

        // -(void)loadOfferWithOfferID:(NSInteger)offerID completion:(void (^ _Nullable)(OfferResponseProxy * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("loadOfferWithOfferID:completion:")]
        void LoadOfferWithOfferID(nint offerID, [NullAllowed] Action<OfferResponseProxy, SDKApiErrorProxy> completion);

        // -(void)loadOffersWithCompletion:(void (^ _Nullable)(NSArray<OfferResponseProxy *> * _Nullable, SDKApiErrorProxy * _Nullable))completion;
        [Export("loadOffersWithCompletion:")]
        void LoadOffersWithCompletion([NullAllowed] Action<NSArray<OfferResponseProxy>, SDKApiErrorProxy> completion);

        // -(void)markOfferAsReadWithOfferID:(NSInteger)offerID completion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("markOfferAsReadWithOfferID:completion:")]
        void MarkOfferAsReadWithOfferID(nint offerID, [NullAllowed] Action<bool, SDKApiErrorProxy> completion);

        // -(void)markOfferAsReceivedWithOfferID:(NSInteger)offerID completion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("markOfferAsReceivedWithOfferID:completion:")]
        void MarkOfferAsReceivedWithOfferID(nint offerID, [NullAllowed] Action<bool, SDKApiErrorProxy> completion);

        // -(void)markOfferAsRedeemedWithOfferID:(NSInteger)offerID completion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("markOfferAsRedeemedWithOfferID:completion:")]
        void MarkOfferAsRedeemedWithOfferID(nint offerID, [NullAllowed] Action<bool, SDKApiErrorProxy> completion);

        // -(void)markOfferAsClickedWithOfferID:(NSInteger)offerID completion:(void (^ _Nullable)(BOOL, SDKApiErrorProxy * _Nullable))completion;
        [Export("markOfferAsClickedWithOfferID:completion:")]
        void MarkOfferAsClickedWithOfferID(nint offerID, [NullAllowed] Action<bool, SDKApiErrorProxy> completion);
    }


    // @interface OfferResponseProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy18OfferResponseProxy")]
    [DisableDefaultCtor]
    interface OfferResponseProxy : INativeObject
    {
        // @property (readonly, nonatomic) BOOL isRedeemable;
        [Export("isRedeemable")]
        bool IsRedeemable { get; }

        // @property (readonly, nonatomic) BOOL isRedeemed;
        [Export("isRedeemed")]
        bool IsRedeemed { get; }

        // @property (readonly, nonatomic) BOOL isVoucher;
        [Export("isVoucher")]
        bool IsVoucher { get; }

        // @property (readonly, nonatomic) NSInteger messageId;
        [Export("messageId")]
        nint MessageId { get; }

        // @property (readonly, nonatomic) NSInteger offerId;
        [Export("offerId")]
        nint OfferId { get; }

        // @property (readonly, nonatomic) NSInteger tileAmount;
        [Export("tileAmount")]
        nint TileAmount { get; }

        // @property (readonly, nonatomic, strong) ButtonModelProxy * _Nullable callToAction;
        [NullAllowed, Export("callToAction", ArgumentSemantic.Strong)]
        ButtonModelProxy CallToAction { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable imageUrl;
        [NullAllowed, Export("imageUrl")]
        string ImageUrl { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable text;
        [NullAllowed, Export("text")]
        string Text { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable thumbnailUrl;
        [NullAllowed, Export("thumbnailUrl")]
        string ThumbnailUrl { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable title;
        [NullAllowed, Export("title")]
        string Title { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable validity;
        [NullAllowed, Export("validity")]
        string Validity { get; }
    }

    // @interface ButtonModelProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtCC16BeaconsmindProxy18OfferResponseProxy16ButtonModelProxy")]
    [DisableDefaultCtor]
    interface ButtonModelProxy
    {
        // @property (readonly, copy, nonatomic) NSString * _Nullable link;
        [NullAllowed, Export("link")]
        string Link { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable title;
        [NullAllowed, Export("title")]
        string Title { get; }
    }

    // @interface ProfileResponseProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy20ProfileResponseProxy")]
    [DisableDefaultCtor]
    interface ProfileResponseProxy
    {
        // @property (readonly, nonatomic) BOOL disablePushNotifications;
        [Export("disablePushNotifications")]
        bool DisablePushNotifications { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull firstName;
        [Export("firstName")]
        string FirstName { get; }

        // @property (readonly, copy, nonatomic) NSDate * _Nonnull joinDate;
        [Export("joinDate", ArgumentSemantic.Copy)]
        NSDate JoinDate { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nonnull lastName;
        [Export("lastName")]
        string LastName { get; }

        // @property (readonly, nonatomic) BOOL newsLetterSubscription;
        [Export("newsLetterSubscription")]
        bool NewsLetterSubscription { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable avatarThumbnailUrl;
        [NullAllowed, Export("avatarThumbnailUrl")]
        string AvatarThumbnailUrl { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable avatarUrl;
        [NullAllowed, Export("avatarUrl")]
        string AvatarUrl { get; }

        // @property (readonly, copy, nonatomic) NSDate * _Nullable birthDate;
        [NullAllowed, Export("birthDate", ArgumentSemantic.Copy)]
        NSDate BirthDate { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable city;
        [NullAllowed, Export("city")]
        string City { get; }

        // @property (readonly, copy, nonatomic) NSArray<NSString *> * _Nullable claims;
        [NullAllowed, Export("claims", ArgumentSemantic.Copy)]
        string[] Claims { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable clubId;
        [NullAllowed, Export("clubId")]
        string ClubId { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable country;
        [NullAllowed, Export("country")]
        string Country { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable favoriteStore;
        [NullAllowed, Export("favoriteStore")]
        string FavoriteStore { get; }

        // @property (readonly, nonatomic, strong) NSNumber * _Nullable favoriteStoreId;
        [NullAllowed, Export("favoriteStoreId", ArgumentSemantic.Strong)]
        NSNumber FavoriteStoreId { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable fullName;
        [NullAllowed, Export("fullName")]
        string FullName { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable gender;
        [NullAllowed, Export("gender")]
        string Gender { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable houseNumber;
        [NullAllowed, Export("houseNumber")]
        string HouseNumber { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable id;
        [NullAllowed, Export("id")]
        string Id { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable landlinePhone;
        [NullAllowed, Export("landlinePhone")]
        string LandlinePhone { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable language;
        [NullAllowed, Export("language")]
        string Language { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable phoneNumber;
        [NullAllowed, Export("phoneNumber")]
        string PhoneNumber { get; }

        // @property (readonly, copy, nonatomic) NSArray<NSString *> * _Nullable roles;
        [NullAllowed, Export("roles", ArgumentSemantic.Copy)]
        string[] Roles { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable street;
        [NullAllowed, Export("street")]
        string Street { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable url;
        [NullAllowed, Export("url")]
        string Url { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable userName;
        [NullAllowed, Export("userName")]
        string UserName { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable zipCode;
        [NullAllowed, Export("zipCode")]
        string ZipCode { get; }
    }

    // @interface SDKApiErrorProxy : NSObject
    [BaseType(typeof(NSObject), Name = "_TtC16BeaconsmindProxy16SDKApiErrorProxy")]
    [DisableDefaultCtor]
    [Protocol]
    interface SDKApiErrorProxy
    {
        // @property (readonly, nonatomic) enum ErrorType errorType;
        [Export("errorType")]
        ErrorType ErrorType { get; }

        // @property (readonly, nonatomic) NSError * _Nullable error;
        [NullAllowed, Export("error")]
        NSError Error { get; }

        // @property (readonly, nonatomic, strong) NSNumber * _Nullable httpStatusCode;
        [NullAllowed, Export("httpStatusCode", ArgumentSemantic.Strong)]
        NSNumber HttpStatusCode { get; }

        // @property (readonly, copy, nonatomic) NSString * _Nullable httpPayload;
        [NullAllowed, Export("httpPayload")]
        string HttpPayload { get; }
    }
}
