using System.Threading.Tasks;
using BeaconsmindSdk.Android.Sdk;
using BeaconsmindSdk.SdkModels.Responses;
using Com.Beaconsmind.Sdk;
using Java.Lang;
using Error = Com.Beaconsmind.Sdk.Error;
using OfferResponseAndroid = Com.Beaconsmind.Api.Models.OfferResponse;

namespace BeaconsmindSdk.Android.Listeners
{
    public class OnOfferResponseListener : Object, IOnResultListener
    {
        private readonly TaskCompletionSource<ResponseBase<OfferResponse>> _tsc;
        
        public OnOfferResponseListener(TaskCompletionSource<ResponseBase<OfferResponse>> tsc)
        {
            _tsc = tsc;
        }
        
        public void ResultListenerOnError(Error error)
        {
            _tsc.TrySetResult(ResponseBase<OfferResponse>.FromError("Request failed"));
        }

        public void OnSuccess(Object offerResponse)
        {
            var offerResponseMapped = Mappers.Map((OfferResponseAndroid)offerResponse);
            _tsc.TrySetResult(ResponseBase<OfferResponse>.FromData(offerResponseMapped));
        }
    }
}