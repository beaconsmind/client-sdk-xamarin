using System.Collections.Generic;
using System.Threading.Tasks;
using BeaconsmindSdk.Android.Sdk;
using BeaconsmindSdk.SdkModels.Responses;
using Com.Beaconsmind.Sdk;
using Java.Interop;
using Java.Lang;
using Java.Util;
using Error = Com.Beaconsmind.Sdk.Error;
using OfferResponseAndroid = Com.Beaconsmind.Api.Models.OfferResponse;
using OfferResponse = BeaconsmindSdk.SdkModels.Responses.OfferResponse;

namespace BeaconsmindSdk.Android.Listeners
{
    public class OnOffersListResponseListener : Object, IOnResultListener
    {
        private readonly TaskCompletionSource<ResponseBase<OfferListResponse>> _tsc;
        
        public OnOffersListResponseListener(TaskCompletionSource<ResponseBase<OfferListResponse>> tsc)
        {
            _tsc = tsc;
        } 
        public void ResultListenerOnError(Error error)
        {
            _tsc.TrySetResult(ResponseBase<OfferListResponse>.FromError("Request failed"));
        }

        public void OnSuccess(Object offersResponse)
        {
            var offersJavaArrayList = offersResponse.JavaCast<ArrayList>();

            var iterator = offersJavaArrayList.Iterator();

            var offers = new OfferListResponse()
            {
                Offers = new List<OfferResponse>()
            };
            
            while (iterator.HasNext)
            {
                offers.Offers.Add(Mappers.Map((OfferResponseAndroid)iterator.Next()));
            }
             
            _tsc.TrySetResult(ResponseBase<OfferListResponse>.FromData(offers));
        }
    }
}