using System.Threading.Tasks;
using BeaconsmindSdk.Android.Sdk;
using BeaconsmindSdk.SdkModels.Responses;
using Com.Beaconsmind.Sdk;
using Java.Lang;
using Error = Com.Beaconsmind.Sdk.Error;
using ProfileResponseAndroid = Com.Beaconsmind.Api.Models.ProfileResponse;

namespace BeaconsmindSdk.Android.Listeners
{
    public class OnProfileResponseListener : Object, IOnResultListener
    {
        private readonly TaskCompletionSource<ResponseBase<ProfileResponse>> _tsc;
        
        public OnProfileResponseListener(TaskCompletionSource<ResponseBase<ProfileResponse>> tsc)
        {
            _tsc = tsc;
        }
        
        public void ResultListenerOnError(Error error)
        {
            _tsc.TrySetResult(ResponseBase<ProfileResponse>.FromError("Request failed"));
        }

        public void OnSuccess(Object profileResponse)
        {
            var profileResponseMapped = Mappers.Map((ProfileResponseAndroid)profileResponse);
            
            _tsc.TrySetResult(ResponseBase<ProfileResponse>.FromData(profileResponseMapped));
        }
    }
}