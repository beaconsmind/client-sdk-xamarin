using System.Threading.Tasks;
using BeaconsmindSdk.SdkModels.Responses;
using Com.Beaconsmind.Sdk;
using Error = Com.Beaconsmind.Sdk.Error;

namespace BeaconsmindSdk.Android.Listeners
{
    public class OnCompleteListener : Java.Lang.Object, IOnCompleteListener
    {
        private readonly TaskCompletionSource<ResponseBase<bool>> _tsc;
        
        public OnCompleteListener(TaskCompletionSource<ResponseBase<bool>> tsc)
        {
            _tsc = tsc;
        }
        
        public void CompleteListenerOnError(Error error)
        {
            _tsc.TrySetResult(ResponseBase<bool>.FromError("Request failed"));
        }
    
        public void OnSuccess()
        {
            _tsc.TrySetResult(ResponseBase<bool>.FromData(true));
        }
    }
}