using System;

namespace BeaconsmindSdk.Android.Extensions
{
    public static class DateTimeExtension
    {
        public static Java.Util.Date ToJavaDate(this DateTime? date)
        {
            if (!date.HasValue)
            {
                return null;
            }
            
            var javaTicks = date.Value.Ticks / TimeSpan.TicksPerMillisecond;
            return new Java.Util.Date(javaTicks);
        }
    }
}