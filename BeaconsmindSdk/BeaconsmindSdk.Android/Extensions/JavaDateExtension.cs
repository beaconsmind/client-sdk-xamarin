using System;

namespace BeaconsmindSdk.Android.Extensions
{
    public static class JavaDateExtension
    {
        public static DateTime? ToDateTime(this Java.Util.Date date)
        {
            if (date == null)
            {
                return null;
            }
            
            var cSharpTicks = date.Time * TimeSpan.TicksPerMillisecond;
            return new DateTime(cSharpTicks, DateTimeKind.Utc);
        }
    }
}