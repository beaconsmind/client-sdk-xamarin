using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.Content;
using BeaconsmindSdk.Android.Listeners;
using BeaconsmindSdk.SdkModels.Requests;
using BeaconsmindSdk.SdkModels.Responses;
using Com.Beaconsmind.Sdk;
using Xamarin.Forms;
using Application = Android.App.Application;
using BeaconContactsSummaryAndroid = Com.Beaconsmind.Sdk.Models.BeaconContactsSummary;
using BeaconContactsSummary = BeaconsmindSdk.SdkModels.Responses.BeaconContactsSummary;


[assembly: Dependency(typeof(BeaconsmindSdk.Android.Sdk.BeaconsmindSdk))]
namespace BeaconsmindSdk.Android.Sdk
{
    public class BeaconsmindSdk : IBeaconsmindSdk
    {
        private readonly Context _context = Application.Context;
        
        public BeaconsmindSdk()
        {
        }
        
        public void SetMinLogLevel(SdkModels.Enums.LogLevel logLevel)
        {
            Com.Beaconsmind.Sdk.BeaconsmindSdk.MinLogLevel = Mappers.Map(logLevel);
        }

        public bool Initialize(SdkModels.BeaconsmindConfig config)
        {
            var builder = new BeaconsmindConfig.Builder(config.SuiteUri);

            builder.UsePassiveScanning(config.UsePassiveScanning) 
                .SetNotificationBadge(config.NotificationBadge) 
                .SetNotificationTitle(config.NotificationTitle)
                .SetNotificationText(config.NotificationText)
                .SetNotificationChannelName(config.NotificationChannelName);

            return Com.Beaconsmind.Sdk.BeaconsmindSdk.Initialize((Application)_context, builder.Build(_context));
        }

        public Task<ResponseBase<bool>> SignUp(CreateUserRequest userRequest)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();
         
            UserManager.Instance.CreateAccount(Mappers.Map(userRequest), new OnCompleteListener(tcs));
            
            return tcs.Task;
        }

        public Task<ResponseBase<bool>> Login(string username, string password)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();
            
            UserManager.Instance.Login(username, password, new OnCompleteListener(tcs));
            
            return tcs.Task;
        }

        public Task<ResponseBase<bool>> Logout()
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            UserManager.Instance.Logout(new OnCompleteListener(tcs));
            
            return tcs.Task;
        }

        public void UpdateHostName(string suiteUri)
        {
            Com.Beaconsmind.Sdk.BeaconsmindSdk.UpdateSuiteUri(suiteUri);
        }

        public void StartListeningBeacons()
        {
            BeaconListenerService.Instance.StartListeningBeacons(null);
        }

        public void StopListeningBeacons()
        {
            BeaconListenerService.Instance.StopListeningBeacons();
        }

        public List<BeaconContactsSummary> GetBeaconsSummary()
        {
            var  beaconSummary = BeaconListenerService.Instance.BeaconsSummary;
            
            var beacons = beaconSummary.ToEnumerable<BeaconContactsSummaryAndroid>()!.Select(Mappers.Map).ToList();

            return beacons;
        }

        public Task<ResponseBase<ProfileResponse>> GetAccount()
        { 
            var tcs = new TaskCompletionSource<ResponseBase<ProfileResponse>>();

            UserManager.Instance.GetAccount(new OnProfileResponseListener(tcs));
            
            return tcs.Task;
        }
        
        public Task<ResponseBase<bool>> UpdateAccount(UpdateProfileRequest userRequest)
        { 
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            UserManager.Instance.UpdateAccount(Mappers.Map(userRequest), new OnCompleteListener(tcs));
            
            return tcs.Task;
        }

        public Task<ResponseBase<OfferListResponse>> LoadOffers()
        {
            var tcs = new TaskCompletionSource<ResponseBase<OfferListResponse>>();

            OffersManager.Instance.LoadOffers(new OnOffersListResponseListener(tcs));

            return tcs.Task;
        }

        public Task<ResponseBase<OfferResponse>> LoadOffer(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<OfferResponse>>();

            OffersManager.Instance.LoadOffer(offerId, new OnOfferResponseListener(tcs));
            
            return tcs.Task;
        }

        public Task<ResponseBase<bool>> MarkOfferAsRead(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            OffersManager.Instance.MarkOfferAsRead(offerId, new OnCompleteListener(tcs));
            
            return tcs.Task;
        }
        
        public void MarkOfferAsReceived(int offerId)
        {
            OffersManager.Instance.MarkOfferAsReceived(offerId);
        }

        public Task<ResponseBase<bool>> MarkOfferAsRedeemed(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            OffersManager.Instance.MarkOfferRedeemed(offerId, new OnCompleteListener(tcs));
            
            return tcs.Task;
        }
        
        public Task<ResponseBase<bool>> MarkOfferAsClicked(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            OffersManager.Instance.MarkOfferClicked(offerId, new OnCompleteListener(tcs));
            
            return tcs.Task;
        }

        public Task<ResponseBase<bool>> ImportAccount(ImportUserRequest userRequest)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();
            
            UserManager.Instance.ImportAccount(Mappers.Map(userRequest), new OnCompleteListener(tcs));
            
            return tcs.Task;
        }
        
        public string GetAccessToken()
        {
            return UserManager.Instance.AccessToken;
        }
    }
}