using System.Collections.Generic;
using System.Linq;
using BeaconsmindSdk.Android.Extensions;
using BeaconsmindSdk.SdkModels.Enums;
using BeaconsmindSdk.SdkModels.Responses;
using OfferResponseAndroid = Com.Beaconsmind.Api.Models.OfferResponse;
using OfferResponse = BeaconsmindSdk.SdkModels.Responses.OfferResponse;
using ProfileResponseAndroid = Com.Beaconsmind.Api.Models.ProfileResponse;
using ProfileResponse = BeaconsmindSdk.SdkModels.Responses.ProfileResponse;
using BeaconContactsSummaryAndroid = Com.Beaconsmind.Sdk.Models.BeaconContactsSummary;
using BeaconContactsSummary = BeaconsmindSdk.SdkModels.Responses.BeaconContactsSummary;
using CreateUserRequest = BeaconsmindSdk.SdkModels.Requests.CreateUserRequest;
using CreateUserRequestAndroid = Com.Beaconsmind.Api.Models.CreateUserRequest;
using ImportUserRequest = BeaconsmindSdk.SdkModels.Requests.ImportUserRequest;
using ImportUserRequestAndroid = Com.Beaconsmind.Api.Models.ImportUserRequest;
using UpdateProfileRequest = BeaconsmindSdk.SdkModels.Requests.UpdateProfileRequest;
using UpdateProfileRequestAndroid = Com.Beaconsmind.Api.Models.UpdateProfileRequest;
using LogLevelAndroid = Com.Beaconsmind.Sdk.Models.LogLevel;

namespace BeaconsmindSdk.Android.Sdk
{
    public static class Mappers
    {
        
        public static LogLevelAndroid Map(LogLevel logLevel) => logLevel switch
        {
            LogLevel.Debug  => LogLevelAndroid.Debug,
            LogLevel.Info  => LogLevelAndroid.Info,
            LogLevel.Warning  => LogLevelAndroid.Warning,
            LogLevel.Error  => LogLevelAndroid.Error,
            _ => LogLevelAndroid.Silent,
        };
        
        public static CreateUserRequestAndroid Map(CreateUserRequest request)
        {
            return new CreateUserRequestAndroid
            {
                Username = request.UserName,
                Password = request.Password,
                FirstName = request.FirstName,
                LastName = request.LastName,
                ConfirmPassword = request.ConfirmPassword,
                Language = request.Language,
                CountryCode = request.CountryCode,
                Gender = request.Gender,
                FavoriteStore = request.FavoriteStore,
                FavoriteStoreId =Java.Lang.Integer.ValueOf(request.FavoriteStoreId),
                Birthday = request.Birthday.ToJavaDate(),
                AvatarUrl = request.AvatarUrl,
                AvatarThumbnailUrl = request.AvatarThumbnailUrl,
                IsAdministrator = Java.Lang.Boolean.ValueOf(request.IsAdministrator),
                IsSuperAdministrator = Java.Lang.Boolean.ValueOf(request.IsSuperAdministrator)
            };
        }
        
        public static UpdateProfileRequestAndroid Map(UpdateProfileRequest request)
        {
            return new UpdateProfileRequestAndroid
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Gender = request.Gender,
                BirthDate = request.BirthDate.ToJavaDate(),
                Street = request.Street,
                HouseNumber = request.HouseNumber,
                ZipCode = request.ZipCode,
                City = request.City,
                Country = request.Country,
                Language = request.Language,
                LandlinePhone = request.LandlinePhone,
                PhoneNumber = request.PhoneNumber, 
                FavoriteStore = request.FavoriteStore,
                FavoriteStoreId = request.FavoriteStoreId.HasValue ? Java.Lang.Integer.ValueOf(request.FavoriteStoreId.Value) : Java.Lang.Integer.ValueOf(-1),
                AvatarUrl = request.AvatarUrl,
                AvatarThumbnailUrl = request.AvatarThumbnailUrl,
                NewsLetterSubscription = request.NewsLetterSubscription.ToString(),
                DisablePushNotifications = request.DisablePushNotifications.ToString(),
            };
        }

        public static ImportUserRequestAndroid Map(ImportUserRequest request)
        {
            return new ImportUserRequestAndroid
            {
                Id = request.Id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                BirthDate = request.BirthDate.ToJavaDate(),
                Language = request.Language,
                Gender = request.Gender
            };
        }
        
        public static ProfileResponse Map(ProfileResponseAndroid model)
        {
            return new ProfileResponse
            {
                Url = model.Url,
                Id = model.Id,
                UserName = model.UserName,
                FullName = model.FullName,
                FavoriteStore = model.FavoriteStore,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Gender = model.Gender,
                Language = model.Language,
                Street = model.Street,
                HouseNumber = model.HouseNumber,
                ZipCode = model.ZipCode,
                City = model.City,
                Country = model.Country,
                LandlinePhone = model.LandlinePhone,
                PhoneNumber = model.PhoneNumber,
                BirthDate = model.BirthDate.ToDateTime(),
                FavoriteStoreId = model.FavoriteStoreId?.IntValue(),
                AvatarUrl = model.AvatarUrl,
                AvatarThumbnailUrl = model.AvatarThumbnailUrl,
                ClubId = model.ClubId,
                DisablePushNotifications = model.DisablePushNotifications.BooleanValue(),
                NewsLetterSubscription = model.NewsLetterSubscription.BooleanValue(),
                JoinDate = model.JoinDate.ToDateTime(),
                Roles = model.Roles.ToList(),
                Claims = model.Claims.ToList()
            };
        }
        
        public static OfferResponse Map(OfferResponseAndroid model)
        {
            return new OfferResponse
            {
                OfferId = model.OfferId.IntValue(),
                MessageId = model.MessageId.IntValue(),
                Title = model.Title,
                Text = model.Text,
                ImageUrl = model.ImageUrl,
                ThumbnailUrl = model.ThumbnailUrl,
                Validity = model.Validity,
                IsRedeemed = model.IsRedeemed?.BooleanValue(),
                IsRedeemable = model.IsRedeemable?.BooleanValue(),
                IsVoucher = model.IsVoucher?.BooleanValue(),
                TileAmount = model.TileAmount?.IntValue(),
                CallToAction = model.CallToAction != null ? new ButtonModel
                {
                    Title = model.CallToAction.Title,
                    Link = model.CallToAction.Link
                } : null
            };
        }
        
        public static OfferListResponse Map(IEnumerable<OfferResponseAndroid> model)
        {
            var offers = new OfferListResponse
            {
                Offers = model.Select(Map).ToList()
            };

            return offers;
        }

        public static BeaconContactsSummary Map(BeaconContactsSummaryAndroid model)
        {
            return new BeaconContactsSummary()
            {
                Store = model.Store,
                Name = model.Name,
                Uuid = model.Uuid,
                Major = model.Major,
                Minor = model.Minor,
                AverageRssi = model.AverageRssi?.DoubleValue(),
                CurrentRssi = model.CurrentRssi?.DoubleValue(),
                TimesContacted = model.TimesContacted,
                LastContact = model.LastContact?.LongValue(),
                IsInRange = model.IsInRange
            };
        }
    }
}