using System;
using Foundation;

namespace BeaconsmindSdk.iOS.Extensions
{
    public static class DateTimeExtension
    {
        public static NSDate ToNSDate(this DateTime? date)
        {
            return !date.HasValue ? null : NSDate.FromTimeIntervalSinceReferenceDate((date.Value.ToUniversalTime() - new DateTime(2001, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds);
        }
    }
}