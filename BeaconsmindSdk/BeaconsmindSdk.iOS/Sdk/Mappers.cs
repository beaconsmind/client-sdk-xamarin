using System.Linq;
using BeaconsmindSdk.SdkModels.Enums;
using BeaconsmindSdk.SdkModels.Responses;
using Binding;
using Foundation;
using Xamarin.Forms.Platform.iOS;

namespace BeaconsmindSdk.iOS.Sdk
{
    public static class Mappers
    {
        public static LogLevelProxy Map(LogLevel logLevel) => logLevel switch
        {
            LogLevel.Debug  => LogLevelProxy.Debug,
            LogLevel.Info  => LogLevelProxy.Info,
            LogLevel.Warning  => LogLevelProxy.Warning,
            LogLevel.Error  => LogLevelProxy.Error,
            _ => LogLevelProxy.Silent,
        };
        
        public static ProfileResponse Map(ProfileResponseProxy responseProxy)
        {
            return new ProfileResponse
            {
                Url = responseProxy.Url,
                Id = responseProxy.Id,
                UserName = responseProxy.UserName,
                FullName = responseProxy.FullName,
                FavoriteStore = responseProxy.FavoriteStore,
                FirstName = responseProxy.FirstName,
                LastName = responseProxy.LastName,
                Gender = responseProxy.Gender,
                Language = responseProxy.Language,
                Street = responseProxy.Street,
                HouseNumber = responseProxy.HouseNumber,
                ZipCode = responseProxy.ZipCode,
                City = responseProxy.City,
                Country = responseProxy.Country,
                LandlinePhone = responseProxy.LandlinePhone,
                PhoneNumber = responseProxy.PhoneNumber,
                BirthDate = responseProxy.BirthDate?.ToDateTime(),
                FavoriteStoreId = responseProxy.FavoriteStoreId?.Int32Value,
                AvatarUrl = responseProxy.AvatarUrl,
                AvatarThumbnailUrl = responseProxy.AvatarThumbnailUrl,
                ClubId = responseProxy.ClubId,
                DisablePushNotifications = responseProxy.DisablePushNotifications,
                NewsLetterSubscription = responseProxy.NewsLetterSubscription,
                JoinDate = responseProxy.JoinDate.ToDateTime(),
                Roles = responseProxy.Roles?.ToList(),
                Claims = responseProxy.Claims?.ToList()
            };
        }

        public static OfferListResponse Map(NSArray<OfferResponseProxy> responseProxy)
        {
            var offers = new OfferListResponse
            {
                Offers = responseProxy.ToArray().Select(Map).ToList()
            };

            return offers;
        }

        public static OfferResponse Map(OfferResponseProxy responseProxy)
        {
            return new OfferResponse
            {
                OfferId = (int)responseProxy.OfferId,
                MessageId = (int)responseProxy.MessageId,
                Title = responseProxy.Title,
                Text = responseProxy.Text,
                ImageUrl = responseProxy.ImageUrl,
                ThumbnailUrl = responseProxy.ThumbnailUrl,
                Validity = responseProxy.Validity,
                IsRedeemed = responseProxy.IsRedeemed,
                IsRedeemable = responseProxy.IsRedeemable,
                IsVoucher = responseProxy.IsVoucher,
                TileAmount = (int)responseProxy.TileAmount,
                CallToAction = responseProxy.CallToAction != null
                    ? new ButtonModel
                    {
                        Title = responseProxy.CallToAction.Title,
                        Link = responseProxy.CallToAction.Link
                    }
                    : null
            };
        }
        
        public static BeaconContactsSummary Map(BeaconContactsSummaryProxy model)
        {
            return new BeaconContactsSummary
            {
                Store = model.Store,
                Name = model.Name,
                Uuid = model.Uuid,
                Major = model.Major,
                Minor = model.Minor,
                AverageRssi = model.AverageRSSI?.DoubleValue,
                CurrentRssi = model.Rssi?.DoubleValue,
                TimesContacted = (int)model.ContactCount,
                LastContact = model.Timestamp?.LongValue,
                IsInRange = model.IsInRange
            };
        }
    }
}