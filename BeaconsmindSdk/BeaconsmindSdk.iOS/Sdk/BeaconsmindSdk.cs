using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BeaconsmindSdk.iOS.Extensions;
using BeaconsmindSdk.SdkModels;
using BeaconsmindSdk.SdkModels.Enums;
using BeaconsmindSdk.SdkModels.Requests;
using BeaconsmindSdk.SdkModels.Responses;
using Binding;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(BeaconsmindSdk.iOS.Sdk.BeaconsmindSdk))]
namespace BeaconsmindSdk.iOS.Sdk
{
    public class Delegate : BeaconsmindDelegateProxy
    {
        public override void OnContextChanged(BeaconsmindProxy beaconsmind, APIContextProxy context)
        {
        }
    }

    public class BeaconsmindSdk : IBeaconsmindSdk
    {
        public void SetMinLogLevel(LogLevel logLevel)
        {
            BeaconsmindProxy.Default_.SetMinLogLevelWithLevel(Mappers.Map(logLevel));
        }

        bool IBeaconsmindSdk.Initialize(BeaconsmindConfig config)
        {
            return BeaconsmindProxy.Default_.StartWithDelegate(new Delegate(), config.AppVersion, config.SuiteUri);
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.SignUp(CreateUserRequest userRequest)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();
         
            BeaconsmindProxy.Default_.SignUpWithUsername(userRequest.UserName, userRequest.FirstName, userRequest.LastName, userRequest.Password,
                userRequest.ConfirmPassword, userRequest.AvatarThumbnailUrl, userRequest.AvatarUrl, userRequest.Language, userRequest.Gender, 
                userRequest.FavoriteStoreId, userRequest.Birthday.ToNSDate(), (proxy, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });            
            return tcs.Task;
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.Login(string username, string password)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();
            
            BeaconsmindProxy.Default_.LoginWithUsername(username, password, (proxy, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });
            
            return tcs.Task;
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.Logout()
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.LogoutWithCompletion((b, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });
            
            return tcs.Task;
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.ImportAccount(ImportUserRequest userRequest)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.ImportAccountWithId(userRequest.Id, userRequest.Email, userRequest.FirstName, userRequest.LastName, 
                userRequest.BirthDate.ToNSDate(), userRequest.Language, userRequest.Gender, 
                (proxy, errorProxy) =>
                {
                    SetResponseBase(tcs, true, errorProxy);
                });
            
            return tcs.Task;
        }

        public void UpdateHostName(string suiteUri)
        {
            BeaconsmindProxy.Default_.UpdateHostnameWithHostname(suiteUri);
        }

        public void StartListeningBeacons()
        {
            BeaconsmindProxy.Default_.StartListeningBeacons();
        }

        public void StopListeningBeacons()
        {
            BeaconsmindProxy.Default_.StopListeningBeacons();
        }

        public List<BeaconContactsSummary> GetBeaconsSummary()
        {
            var  beaconSummary = BeaconsmindProxy.Default_.BeaconsContactsSummary;

            var beacons = beaconSummary.ToList().Select(Mappers.Map).ToList();
            
            return beacons;
        }

        Task<ResponseBase<ProfileResponse>> IBeaconsmindSdk.GetAccount()
        {
            var tcs = new TaskCompletionSource<ResponseBase<ProfileResponse>>();
            
            BeaconsmindProxy.Default_.GetProfileWithCompletion((responseProxy, errorProxy) =>
            {
                SetResponseBase(tcs, Mappers.Map(responseProxy), errorProxy);
            });
            
            return tcs.Task;
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.UpdateAccount(UpdateProfileRequest userRequest)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.UpdateProfileWithFirstName(userRequest.FirstName, userRequest.LastName, userRequest.AvatarThumbnailUrl, userRequest.AvatarUrl,
                userRequest.BirthDate.ToNSDate(), userRequest.City, userRequest.Country, new NSNumber(userRequest.DisablePushNotifications), 
                userRequest.FavoriteStoreId == null ? null : new NSNumber(userRequest.FavoriteStoreId.Value), userRequest.Gender,
                userRequest.HouseNumber, userRequest.LandlinePhone, userRequest.Language, userRequest.PhoneNumber, userRequest.Street, userRequest.ZipCode, 
                (proxy, errorProxy) =>
                {
                    SetResponseBase(tcs, true, errorProxy);
                });
            
            return tcs.Task;
        }

        Task<ResponseBase<OfferListResponse>> IBeaconsmindSdk.LoadOffers()
        {
            var tcs = new TaskCompletionSource<ResponseBase<OfferListResponse>>();

            BeaconsmindProxy.Default_.LoadOffersWithCompletion((array, errorProxy) =>
            {
                SetResponseBase(tcs, Mappers.Map(array), errorProxy);
            });

            return tcs.Task;
        }

        Task<ResponseBase<OfferResponse>> IBeaconsmindSdk.LoadOffer(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<OfferResponse>>();

            BeaconsmindProxy.Default_.LoadOfferWithOfferID(offerId, (offer, errorProxy) =>
            {
                SetResponseBase(tcs, Mappers.Map(offer), errorProxy);
            });

            return tcs.Task;
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.MarkOfferAsRead(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.MarkOfferAsReadWithOfferID(offerId, (array, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });

            return tcs.Task;
        }
        
        public Task<ResponseBase<bool>> MarkOfferAsClicked(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.MarkOfferAsClickedWithOfferID(offerId, (array, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });

            return tcs.Task;
        }

        void IBeaconsmindSdk.MarkOfferAsReceived(int offerId)
        {
            BeaconsmindProxy.Default_.MarkOfferAsReceivedWithOfferID(offerId, null);
        }

        Task<ResponseBase<bool>> IBeaconsmindSdk.MarkOfferAsRedeemed(int offerId)
        {
            var tcs = new TaskCompletionSource<ResponseBase<bool>>();

            BeaconsmindProxy.Default_.MarkOfferAsRedeemedWithOfferID(offerId, (array, errorProxy) =>
            {
                SetResponseBase(tcs, true, errorProxy);
            });

            return tcs.Task;
        }
        
        public string GetAccessToken()
        {
            return BeaconsmindProxy.Default_.APIContext == null ? null : BeaconsmindProxy.Default_.APIContext.UserID;
        }
        
        private static void SetResponseBase<T>(TaskCompletionSource<ResponseBase<T>> tcs, T value, SDKApiErrorProxy errorProxy)
        {
            tcs.TrySetResult(errorProxy != null
                ? ResponseBase<T>.FromError("Request failed")
                : ResponseBase<T>.FromData(value));
        }
    }
}