using System;

namespace BeaconsmindSdk.SdkModels.Requests
{
    public class ImportUserRequest
    {
        public string Id { get; set; }
        public string Email { get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Language { get; set; }
        public string Gender { get; set; }
    }
}