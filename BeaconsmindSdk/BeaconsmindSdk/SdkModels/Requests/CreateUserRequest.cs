using System;

namespace BeaconsmindSdk.SdkModels.Requests
{
    public class CreateUserRequest
    { 
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Language { get; set; }
        public string CountryCode { get; set; }
        public string Gender { get; set; }
        public string FavoriteStore { get; set; }
        public int FavoriteStoreId { get; set; }
        public DateTime? Birthday { get; set; }
        public string AvatarUrl { get; set; }
        public string AvatarThumbnailUrl { get; set; }
        public string DeviceRegistration { get; set; }
        public bool IsAdministrator { get; set; }
        public bool IsSuperAdministrator { get; set; }
    }
}