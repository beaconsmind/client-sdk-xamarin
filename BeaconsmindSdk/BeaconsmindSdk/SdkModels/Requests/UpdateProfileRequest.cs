using System;
using BeaconsmindSdk.SdkModels.Responses;

namespace BeaconsmindSdk.SdkModels.Requests
{
    public class UpdateProfileRequest
    {
        public UpdateProfileRequest(ProfileResponse profile)
        {
            FirstName = profile.FirstName;
            LastName = profile.LastName;
            Gender = profile.Gender;
            BirthDate = profile.BirthDate;
            Street = profile.Street;
            HouseNumber = profile.HouseNumber;
            ZipCode = profile.ZipCode;
            City = profile.City;
            Country = profile.Country;
            Language = profile.Language;
            LandlinePhone = profile.LandlinePhone;
            PhoneNumber = profile.PhoneNumber;
            FavoriteStore = profile.FavoriteStore;
            FavoriteStoreId = profile.FavoriteStoreId;
            AvatarUrl = profile.AvatarUrl;
            AvatarThumbnailUrl = profile.AvatarThumbnailUrl;
            NewsLetterSubscription = profile.NewsLetterSubscription;
            DisablePushNotifications = profile.DisablePushNotifications;
        }

        public string FirstName {get; set;}
        public string LastName {get; set;}
        public string Gender {get; set;}
        public DateTime? BirthDate {get; set;}
        public string Street {get; set;}
        public string HouseNumber {get; set;}
        public string ZipCode {get; set;}
        public string City {get; set;}
        public string Country {get; set;}
        public string Language {get; set;}
        public string LandlinePhone {get; set;}
        public string PhoneNumber {get; set;}
        public string FavoriteStore {get; set;}
        public int? FavoriteStoreId {get; set;}
        public string AvatarUrl {get; set;}
        public string AvatarThumbnailUrl {get; set;}
        public bool NewsLetterSubscription {get; set;}
        public bool DisablePushNotifications {get; set;}
    }
}