namespace BeaconsmindSdk.SdkModels.Responses
{
    public class BeaconContactsSummary
    {
        public string Store { get; set; }
        public string Name { get; set; }
        public string Uuid { get; set; }
        public string Major { get; set; }
        public string Minor { get; set; }
        public double? AverageRssi { get; set; }
        public double? CurrentRssi { get; set; }
        public int TimesContacted { get; set; }
        public long? LastContact { get; set; }
        public bool IsInRange { get; set; }
    }
}