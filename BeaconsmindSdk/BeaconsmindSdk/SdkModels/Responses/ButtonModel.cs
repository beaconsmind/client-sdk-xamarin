namespace BeaconsmindSdk.SdkModels.Responses
{
    public class ButtonModel
    {
        public string Title { get; set; }
        public string Link { get; set; }
    }
}