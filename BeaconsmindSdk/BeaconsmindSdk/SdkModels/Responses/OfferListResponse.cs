using System.Collections.Generic;

namespace BeaconsmindSdk.SdkModels.Responses
{
    public class OfferListResponse 
    {
        public List<OfferResponse> Offers { get; set; }
    }
}