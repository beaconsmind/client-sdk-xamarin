using System;
using System.Collections.Generic;

namespace BeaconsmindSdk.SdkModels.Responses
{
    public class ProfileResponse
    {
        public string Url { get; set; }
        public string Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string FavoriteStore { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Language { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string LandlinePhone { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? BirthDate { get; set; }
        public int? FavoriteStoreId { get; set; }
        public string AvatarUrl { get; set; }
        public string AvatarThumbnailUrl { get; set; }
        public string ClubId { get; set; }
        public bool DisablePushNotifications { get; set; }
        public bool NewsLetterSubscription { get; set; }
        public DateTime? JoinDate { get; set; }
        public List<string> Roles  { get; set; } = null;
        public List<string> Claims  { get; set; } = null;
    }
}