namespace BeaconsmindSdk.SdkModels.Responses
{
    public class OfferResponse 
    {
        public int OfferId { get; set; }
        public int MessageId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string Validity { get; set; }
        public bool? IsRedeemed { get; set; }
        public bool? IsRedeemable { get; set; }
        public bool? IsVoucher { get; set; }
        public int? TileAmount { get; set; }
        public ButtonModel CallToAction { get; set; }
    }
}