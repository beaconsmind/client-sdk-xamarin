namespace BeaconsmindSdk.SdkModels.Responses
{
    public class ResponseBase<T>
    {
        public static ResponseBase<T> FromError(string error)
        {
            return new ResponseBase<T>
            {
                Error = error
            };
        }
        
        public static ResponseBase<T> FromData(T data)
        {
            return new ResponseBase<T>
            {
                Data = data
            };
        }

        public T Data { get; set; }
        
        public string Error { get; set; }
        
        public bool HasErrors
        {
            get => Error != null;
        }
    }
}