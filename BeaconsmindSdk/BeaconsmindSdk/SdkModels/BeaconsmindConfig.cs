namespace BeaconsmindSdk.SdkModels
{
    public class BeaconsmindConfig
    {
        public string SuiteUri { get; set; }
        public string AppVersion { get; set; }
        /// true for scanning without persistent notification
        public bool UsePassiveScanning { get; set; }
        public int NotificationBadge { get; set; }
        public string NotificationTitle { get; set; }
        public string NotificationText { get; set; }
        public string NotificationChannelName { get; set; }
    }
}