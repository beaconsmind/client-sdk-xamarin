namespace BeaconsmindSdk.SdkModels.Enums
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        Silent
    }
}