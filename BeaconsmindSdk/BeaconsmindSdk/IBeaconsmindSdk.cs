using System.Collections.Generic;
using System.Threading.Tasks;
using BeaconsmindSdk.SdkModels;
using BeaconsmindSdk.SdkModels.Enums;
using BeaconsmindSdk.SdkModels.Requests;
using BeaconsmindSdk.SdkModels.Responses;

namespace BeaconsmindSdk
{
    public interface IBeaconsmindSdk
    {
        void SetMinLogLevel(LogLevel logLevel);
        bool Initialize(BeaconsmindConfig config);
        Task<ResponseBase<bool>> SignUp(CreateUserRequest userRequest);
        Task<ResponseBase<bool>> Login(string username, string password);
        Task<ResponseBase<bool>> Logout();
        Task<ResponseBase<bool>> ImportAccount(ImportUserRequest userRequest);
        void UpdateHostName(string suiteUri);
        void StartListeningBeacons();
        void StopListeningBeacons();
        List<BeaconContactsSummary> GetBeaconsSummary();
        Task<ResponseBase<ProfileResponse>> GetAccount();
        Task<ResponseBase<bool>> UpdateAccount(UpdateProfileRequest userRequest);
        Task<ResponseBase<OfferListResponse>> LoadOffers();
        Task<ResponseBase<OfferResponse>> LoadOffer(int offerId);
        Task<ResponseBase<bool>> MarkOfferAsRead(int offerId);
        Task<ResponseBase<bool>> MarkOfferAsClicked(int offerId);
        void MarkOfferAsReceived(int offerId);
        Task<ResponseBase<bool>> MarkOfferAsRedeemed(int offerId);
        string GetAccessToken();
    }
}