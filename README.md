# @beaconsmind/xamarin-sdk

[Beaconsmind](https://beaconsmind.com/) SDK for Xamarin.

## Prerequisites

As a Beaconsmind client, a contained environment is provided for Beaconsmind services being hosted and maintained by
Beaconsmind.

The hostname of the provided environment is required to properly access Beaconsmind API and to use Beaconsmind SDK

> example: `https://adidas.bms.beaconsmind.com`

Please contact Beaconsmind support for the required information about acquiring a valid environment and hostname.

## 1. Installing
Install 'BeaconsmindSdk' NuGet package.

## 2. Permissions

The Beaconsmind SDK requires the following permissions to fully work:
- Notification
- Bluetooth
- Location (whenInUse/always).

In the iOS project you can request permissions by calling these two methods:

```cs
BeaconsmindProxy.Default_.RegisterForPushNotifications
BeaconsmindProxy.Default_.RequestLocationPermissionWithCallback(b => {});
```

## 3. Initialize the SDK
Add a dummy reference in your iOS app so the linker does not remove the assembly Beaconsmind.iOS since it thinks it's not being used.
```cs
var sdk = new global::BeaconsmindSdk.iOS.Sdk.BeaconsmindSdk();
```

Add the lines below to your application's manifest for continuous BeaconListenerService startup.
```xml
<provider android:name="androidx.startup.InitializationProvider" android:authorities="${applicationId}.androidx-startup" android:exported="false" tools:node="merge">
    <meta-data android:name="com.beaconsmind.sdk.BeaconsmindSdkInitializer" android:value="androidx.startup" />
</provider>
```

Get BeaconsmindSdk instance using and Dependency service and register it as singleton.
```cs
var sdkInstance = DependencyService.Get<IBeaconsmindSdk>();
DependencyService.RegisterSingleton(sdkInstance);
```

This will initialize the SDK and try to retrieve the logged-in user. Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.
```cs           
var config = new BeaconsmindConfig()
{
    SuiteUri = DefaultSuiteUri,
    AppVersion = "1",
    UsePassiveScanning = false, 
    NotificationBadge = Resource.Drawable.xamarin_logo,
    NotificationTitle = "Beaconsmind Demo",
    NotificationText = "You are near a beacon",
    NotificationChannelName = "beaconsmind",
};

DependencyService.Get<IBeaconsmindSdk>().Initialize(config); 
```

Make sure your _iOS Deployment Target_ is 11.0 or above.

Make sure your Android _minSdkVersion_ is 23 or above.

See:
* [`Initialize(...)`](#initialize)

## 4. Authentication

### When using Beaconsmind authentication

This approach implies that the customer data is kept in Beaconsmind backend. Use `Login` method to perform
customer authentication.

To login existing user.

```cs
var result = await DependencyService.Get<IBeaconsmindSdk>().Login(email, password);
```

See:
* [`Login(...)`](#login)

To register a new user.

```cs
var request = new CreateUserRequest()
{
    FirstName = FirstName,
    LastName = LastName,
    UserName = Email,
    Password = Password,
    ConfirmPassword = ConfirmPassword
};
            
var result = await DependencyService.Get<IBeaconsmindSdk>().SignUp(request);
```

See:
* [`SignUp(...)`](#signup)

### When using own authentication mechanism

This approach implies that the customer data is kept in clients backend. The authentication is done by the client app
before the Beaconsmind API is used. Whenever customer is authenticated, use `ImportAccount` method
to send basic customer data to Beaconsmind. The customer data is updated only the first time the method is called. The
client app can track if the customer was imported before and omit personal data from following importAccount calls.

```cs
var request = new CreateUserRequest()
{
    Id = Id,
    Email = Email,
    FirstName = FirstName,
    LastName = LastName,
    BirthDate = BirthDate,
    Language = Language,
    Gender = Gender
};

var result = await DependencyService.Get<IBeaconsmindSdk>().ImportAccount(request);
```

See:
* [`ImportAccount(...)`](#importaccount)

## Log out

In order to log out the current account and stop listening for beacons, call the `Logout` method.

See:
* [`Logout()`](#logout)

## 6. Notifications

The SDK uses _APNS_ for iOS, and _Firebase_ for Android. 

### Setting device token

#### Android
To set device token import `Com.Beaconsmind.Sdk` and call method `UserManager.Instance.UpdateDeviceInfo(string token)`

Example in the DemoApp `CustomFirebaseMessagingService` class.

#### iOS
To set device token import `Binding` and call method 
`BeaconsmindProxy.Default_.RegisterWithDeviceToken(NSData deviceToken,
    PlatformTypeProxy platformType, Action<bool, SDKApiErrorProxy>? completion)`

Example can be found in the DemoApp `AppDelegate.cs` class.

## 7. Offers

Beacons ranging is used to pick up offers. In respect to offers, when users receive pushes about a new offer, read an offer and consume an offer, API calls need to be sent to recognize user activity.

### Getting offers

* [`LoadOffers()`](#loadoffers)
* [`LoadOffer(...)`](#loadoffer)

### Interacting with offers

* [`MarkOfferAsReceived(...)`](#markofferasreceived)
* [`MarkOfferAsRead(...)`](#markofferasread)
* [`MarkOfferAsRedeemed(...)`](#markofferasredeemed)

## 8. Beacons

### Interacting with beacons

Beacons feature uses BLE transmitters which can be picked up by devices. In Beaconsmind, BLE transmitters are used to present offers based on a physical location of a device.

Every beacon is defined by it's UUID and major and minor numbers. A list of beacons is persisted and maintained on Beaconsmind environment for each client.

In order to start monitoring nearby beacon devices, call `StartListeningBeacons()`. Make sure that the bluetooth & location permissions have been granted. To stop the monitoring process, call `StopListeningBeacons()`. In order to obtain a list of beacon devices that are within range, call `Beaconsmind.getBeaconContactsSummary()`.

See:
* [`StartListeningBeacons()`](#startlisteningbeacons)
* [`StopListeningBeacons()`](#stoplisteningbeacons)
* [`GetBeaconContactsSummary()`](#getbeaconcontactssummary)

### Read more about beacons

- [iOS setup](https://gitlab.com/beaconsmind/client-sdk-ios/-/blob/main/Documentation/Beacons.md).
- [Android setup](https://gitlab.com/beaconsmind/client-sdk-android/-/blob/main/Documentation/Beacons.md).


## Deployment
If using Code Shrinker, add the following proguard rules:
```
##---------------Begin: proguard configuration for Gson  ----------
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-dontwarn sun.misc.**
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.google.gson.examples.android.model.** { <fields>; }

# Prevent proguard from stripping interface information from TypeAdapter, TypeAdapterFactory,
# JsonSerializer, JsonDeserializer instances (so they can be used in @JsonAdapter)
-keep class * extends com.google.gson.TypeAdapter
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

# Prevent R8 from leaving Data object members always null
-keepclassmembers,allowobfuscation class * {
@com.google.gson.annotations.SerializedName <fields>;
}

# Retain generic signatures of TypeToken and its subclasses with R8 version 3.0 and higher.
-keep,allowobfuscation,allowshrinking class com.google.gson.reflect.TypeToken
-keep,allowobfuscation,allowshrinking class * extends com.google.gson.reflect.TypeToken

##---------------End: proguard configuration for Gson  ----------
```

## API

<docgen-api>
<!--Update the source file JSDoc comments and rerun docgen to update the docs below-->

### SetMinLogLevel(...)

```cs
void SetMinLogLevel(LogLevel logLevel);
```

Sets the minimum log level.

| Param    | Type                                                            |
|----------| --------------------------------------------------------------- |
| logLevel | <code><a href="#logLevel">LogLevel</a></code> |


--------------------


### Initialize(...)

```cs
 bool Initialize(BeaconsmindConfig config);
```

Initializes the SDK.

Call this method as soon as your app starts, so the sdk can track all touchpoints and beacon events correctly.

| Param         | Type                                                            |
| ------------- |-----------------------------------------------------------------|
| **`config`** | <code><a href="#beaconsmindconfig">BeaconsmindConfig</a></code> |

**Returns:** <code>bool</code>

--------------------

### Login(...)

```cs
Task<ResponseBase> Login(string username, string password);
```

Performs login and immediately starts listening for beacons

| Param          | Type                |
|----------------|---------------------|
| **`username`** | <code>string</code> |
| **`password`** | <code>string</code> |


**Returns:** <code>Task&lt;<a href="#baseresponse">ResponseBase</a>&lt;bool&gt;</a>&gt;</code>

--------------------


### SignUp(...)

```cs
Task<ResponseBase<bool>> SignUp(CreateUserRequest userRequest);
```

Creates a new account login and immediately starts listening for beacons.

| Param             | Type                                                            |
|-------------------|-----------------------------------------------------------------|
| **`userRequest`** | <code><a href="#createuserrequest">CreateUserRequest</a></code> |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;</a>&gt;</code>

--------------------


### ImportAccount(...)

```cs
<ResponseBase<bool>> ImportAccount(ImportUserRequest userRequest);
```

When the customer is using 3rd party authentication and not relying on Beaconsmind to keep the customer's private data,
this endpoint is used to import the customer into Beaconsmind and to obtain a token with which the app can send tracking data to us.
The token does NOT provide access to any personal customer info.
If personal data is present in the request, it will only be updated the first time the endpoint is invoked for that customer.
Once the authentication is done by the app, the username is sent to Beaconsmind using /api/accounts/import to receive a JWT for access to Beaconsmind backend.
In this case, Beaconsmind will not allow any personal data to be accessed and it’s the apps responsibility to only send personal data the first time /api/accounts/import is used.
When using import, editing profile is disabled.

| Param         | Type                                                                  |
| ------------- | --------------------------------------------------------------------- |
| **`options`** | <code><a href="#ImportUserRequest">ImportUserRequest</a></code> |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;&gt;</code>

--------------------


### Logout()

```cs
Task<ResponseBase<bool>> Logout();
```

Logs out the current account and stops listening for beacons.

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;&gt;</code>

--------------------


### GetAccessToken()

```cs
string GetAccessToken();
```

Returns the currently logged in user id.

**Returns:** <code><string</code>

--------------------


### GetAccount()

```cs
ResponseBase<ProfileResponse>> GetAccount();
```

Returns the currently logged in user profile data.

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;<a href="#profileresponse">ProfileResponse</a>&gt;&gt;</code>

--------------------


### UpdateAccount(...)

```cs
<ResponseBase<bool>> UpdateAccount(UpdateProfileRequest userRequest);
```

Updates the currently logged in user profile data.

| Param         | Type                                                                                |
| ------------- |-------------------------------------------------------------------------------------|
| **`userRequest`** | <code>Task&lt;<a href="#UpdateProfileRequest">UpdateProfileRequest</a>&gt;</code>   |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;&gt;</code>

--------------------


### LoadOffers()

```cs
Task<ResponseBase<OfferListResponse>> LoadOffers();
```

Loads the list of offers for the current user.

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;<a href="#offerlistresponse">OfferListResponse</a>&gt;&gt;</code>

--------------------


### LoadOffer(...)

```cs
Task<ResponseBase<OfferResponse>> LoadOffer(int offerId);
```

Loads the offer details for the given offer id.

| Param     | Type             |
| --------- |------------------|
| **`offerId`** | <code>int</code> |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;<a href="#offerresponse">OfferResponse</a>&gt;&gt;</code>

--------------------


### MarkOfferAsReceived(...)

```cs
Task<ResponseBase> MarkOfferAsReceived(int offerId);
```

Marks the offer as received.

Call this when an offer is received via push notification.

| Param     | Type                |
| --------- |---------------------|
| **`offerId`** | <code>int</code>    |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;</a>&gt;</code>

--------------------


### MarkOfferAsRead(...)

```cs
Task<ResponseBase> MarkOfferAsRead(int offerId);
```

Marks the offer as read.

Call this when the user opens the offer.

| Param     | Type             |
| --------- |------------------|
| **`offerId`** | <code>int</code> |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;&gt;</code>

--------------------


### MarkOfferAsRedeemed(...)

```cs
Task<ResponseBase> MarkOfferAsRedeemed(int offerId);
```

Marks the offer as redeemed.

Call this when the user redeems the offer.

| Param         | Type             |
|---------------|------------------|
| **`offerId`** | <code>int</code> |

**Returns:** <code>Task&lt;<a href="#responsebase">ResponseBase</a>&lt;bool&gt;</a>&gt;</code>

--------------------


### StartListeningBeacons()

```cs
void StartListeningBeacons();
```

Starts listening for beacons devices. It requires the following permissions to work:
- location (whenInUse/always)
- bluetooth

--------------------


### StopListeningBeacons()

```cs
void StopListeningBeacons();
```

Stops listening for beacons devices.

--------------------


### UpdateHostname(...)

```cs
void UpdateHostName(string suiteUri);
```

Updates the hostname that the SDK will use to connect to the Beaconsmind servers.

| Param     | Type                |
| --------- |---------------------|
| **`suiteUri`** | <code>string</code> |


--------------------


### GetBeaconsSummary()

```cs
List<BeaconContactsSummary> GetBeaconsSummary();
```

Get a list of all the beacons that are currently in range.

**Returns:** <code>Promise&lt;List&lt;<a href="#beaconcontactssummary">BeaconContactsSummary></a>&gt;</code>

--------------------


### Interfaces


#### BeaconsmindConfig

| Prop                          | Type                |
|-------------------------------|---------------------|
| **`SuiteUri`**                | <code>string</code> |
| **`AppVersion`**              | <code>string</code> |
| **`UsePassiveScanning`**      | <code>bool</code>   |
| **`NotificationBadge`**       | <code>int</code>    |
| **`NotificationTitle`**       | <code>string</code> |
| **`NotificationText`**        | <code>string</code> |
| **`NotificationChannelName`** | <code>string</code> |



#### ResponseBase<T>

| Prop            | Type                |
|-----------------|---------------------|
| **`Data`**      | <code>T</code>      |
| **`Error`**     | <code>string</code> |
| **`HasErrors`** | <code>bool</code>   |



#### CreateUserRequest

| Prop                  | Type                |
|-----------------------| ------------------- |
| **`Username`**        | <code>string</code> |
| **`FirstName`**       | <code>string</code> |
| **`LastName`**        | <code>string</code> |
| **`Password`**        | <code>string</code> |
| **`ConfirmPassword`** | <code>string</code> |
| **`Language`**        | <code>string</code> |
| **`Gender`**          | <code>string</code> |
| **`FavoriteStoreID`** | <code>number</code> |
| **`BirthDateSeconds`** | <code>number</code> |


#### UpdateUserRequest

| Prop                           | Type                    |
|--------------------------------|-------------------------|
| **`FirstName`**                | <code>string</code>     |
| **`LastName`**                 | <code>string</code>     |
| **`Gender`**          | <code>string</code> |
| **`BirthDate`**                | <code>DateTime?</code>  | 
| **`Language`**                 | <code>string</code>     |
| **`Street`**                   | <code>string</code>     |
| **`HouseNumber`**              | <code>string</code>     |
| **`ZipCode`**                  | <code>string</code>     |
| **`City`**                     | <code>string</code>     |
| **`Country`**                  | <code>string</code>     |
| **`LandlinePhone`**            | <code>string</code>     |
| **`PhoneNumber`**              | <code>string</code>     |
| **`FavoriteStore`**            | <code>string</code>     |
| **`FavoriteStoreId`**          | <code>int?</code>       | 
| **`AvatarUrl`**                | <code>string</code>     |
| **`AvatarThumbnailUrl`**       | <code>string</code>     |
| **`DisablePushNotifications`** | <code>string</code>     |
| **`NewsLetterSubscription`**   | <code>string</code>     |


#### ImportAUserRequest

| Prop                   | Type                |
|------------------------| ------------------- |
| **`Id`**               | <code>string</code> |
| **`Email`**            | <code>string</code> |
| **`FirstName`**        | <code>string</code> |
| **`LastName`**         | <code>string</code> |
| **`Language`**         | <code>string</code> |
| **`Gender`**           | <code>string</code> |
| **`BirthDateSeconds`** | <code>number</code> |


#### ProfileResponse

| Prop                           | Type                    |
|--------------------------------|-------------------------|
| **`Url`**                      | <code>string</code>     |
| **`Id`**                       | <code>string</code>     |
| **`UserName`**                 | <code>string</code>     |
| **`FullName`**                 | <code>string</code>     |
| **`FavoriteStore`**            | <code>string</code>     |
| **`FirstName`**                | <code>string</code>     |
| **`LastName`**                 | <code>string</code>     |
| **`Language`**                 | <code>string</code>     |
| **`Street`**                   | <code>string</code>     |
| **`HouseNumber`**              | <code>string</code>     |
| **`ZipCode`**                  | <code>string</code>     |
| **`City`**                     | <code>string</code>     |
| **`Country`**                  | <code>string</code>     |
| **`LandlinePhone`**            | <code>string</code>     |
| **`PhoneNumber`**              | <code>string</code>     |
| **`BirthDate`**                | <code>DateTime?</code>  | 
| **`FavoriteStoreId`**          | <code>int?</code>       | 
| **`AvatarUrl`**                | <code>string</code>     |
| **`AvatarThumbnailUrl`**       | <code>string</code>     |
| **`DisablePushNotifications`** | <code>string</code>     |
| **`NewsLetterSubscription`**   | <code>string</code>     |
| **`JoinDate`**                 | <code>DateTime?</code>  | 
| **`Claims`**                   | <code>string[]</code>   |
| **`Roles`**                    | <code>string[]</code>   |

#### OfferListResponse

| Prop         | Type                           |
| ------------ |--------------------------------|
| **`offers`** | <code>OfferResponse[]</code>   |


#### OfferResponse

| Prop               | Type                       |
|--------------------|----------------------------|
| **`OfferId`**      | <code>int</code>           |
| **`MessageId`**    | <code>int</code>           |
| **`Title`**        | <code>string</code>        |
| **`Text`**         | <code>string</code>        |
| **`ImageUrl`**     | <code>string</code>        |
| **`ThumbnailUrl`** | <code>string</code>        |
| **`Balidity`**     | <code>string</code>        |
| **`IsRedeemed`**   | <code>bool?</code>         |
| **`IsRedeemable`** | <code>bool?</code>         |
| **`IsVoucher`**    | <code>bool?</code>         |
| **`TileAmount`**   | <code>int?</code>          |
| **`CallToAction`** | <code>ButtonModel</code>   | 



#### ButtonModel

| Prop           | Type                |
|----------------|---------------------|
| **`Title`**    | <code>string</code> |
| **`Link`**     | <code>string</code> |


#### BeaconContactsSummary

A class that holds information about the beacon device.

It consists of the beacon unique identifier ([Uuid], [Minor], [Major]),
the beacon's name ([Name]) and the name of the [Store] that it is located in.

Additionally, it contains data related to its signal strength and the number
of times it was contacted.

| Prop                 | Type                 | Description                                                                                                                                                                                                                    |
|----------------------|----------------------| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| **`Uuid`**           | <code>string</code>  | Beacon's uuid.                                                                                                                                                                                                                 |
| **`Major`**          | <code>string</code>  | Beacon's major.                                                                                                                                                                                                                |
| **`Minor`**          | <code>string</code>  | Beacon's minor.                                                                                                                                                                                                                |
| **`Name`**           | <code>string</code>  | Beacon's name.                                                                                                                                                                                                                 |
| **`Store`**          | <code>string</code>  | The name of the store that the beacon is located in.                                                                                                                                                                           |
| **`CurrentRSSI`**    | <code>double?</code> | Beacon's current signal strength in dB. Null if the beacon has not been contacted yet. RSSI is an abbreviation from Received Signal Strength Indicator. See: https://en.wikipedia.org/wiki/Received_signal_strength_indication |
| **`AverageRSSI`**    | <code>double?<code>  | Beacon's average signal strength in dB (based on the last contacts). Null if the beacon has not been contacted yet.                                                                                                            |
| **`LastContact`**    | <code>long</code>    | Last beacon contact timestamp represented as unix timestamp in milliseconds. Null when the beacon has not been contacted yet.                                                                                                  |
| **`TimesContacted`** | <code>number</code>  | Indicates how many times the beacon was contacted.                                                                                                                                                                             |
| **`IsInRange`**      | <code>boolean</code> | Returns if the beacon is within range.                                                                                                                                                                                         |


### Enums

#### LogLevel

| Members       | Value                  | Description                                                                                                            |
|---------------| ---------------------- | ---------------------------------------------------------------------------------------------------------------------- |
| **`Debug`**   | <code>'Debug'</code>   | Any developer-level debug info, repeating events, e.g.: - monitoring/location callbacks, - networking success results. |
| **`Info`**    | <code>'Info'</code>    | Bigger events/steps in the SDK lifecycle, e.g.: - starting/stopping services, - initializing SDK, - one-time events.   |
| **`Warning`** | <code>'Warning'</code> | Allowed, but not-optimal, inconsistent state, e.g.: - trying to start monitoring without permissions.                  |
| **`Error`**   | <code>'Error'</code>   | Not allowed state (any error).                                                                                         |
| **`Silent`**  | <code>'Silent'</code>  | No logging at all.                                                                                                     |

## Developer Notes
Iphone simulator won't work on Mac computers with Apple silicon processors because iOS bindings references fat framework that includes x86_64 architecture for simulators.
If you have Mac computers with Apple silicon processors and want to use simulator for development you need to create your own build and reference it.

Official microsoft documentation with instructions: https://learn.microsoft.com/en-us/xamarin/ios/platform/binding-swift/walkthrough

</docgen-api>
