﻿using System;
using AndroidX.Startup;

namespace Com.Beaconsmind.Sdk
{
    partial class BeaconsmindSdkInitializer
    {
        Java.Lang.Object IInitializer.Create(Android.Content.Context p0)
        {
            return Create(p0);
        }
    }
}

